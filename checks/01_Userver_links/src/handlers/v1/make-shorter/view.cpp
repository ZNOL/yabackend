#include "view.hpp"

#include <fmt/format.h>

#include <chrono>
#include <iostream>
#include <userver/formats/json.hpp>
#include <userver/server/handlers/http_handler_base.hpp>
#include <userver/storages/postgres/cluster.hpp>
#include <userver/storages/postgres/component.hpp>
#include <userver/utils/assert.hpp>
#include <userver/utils/datetime.hpp>
#include <vector>

namespace url_shortener {

    namespace {

        std::chrono::duration<int, std::ratio<1>> correct_time(const std::string &unit,
                                                               int duration) {
            if (unit == "SECONDS")
                return std::chrono::duration < int, std::ratio < 1 >> (duration);
            if (unit == "MINUTES")
                return std::chrono::duration < int, std::ratio < 1 >> (duration * 60);
            if (unit == "HOURS")
                return std::chrono::duration < int, std::ratio < 1 >> (duration * 60 * 60);
            if (unit == "DAYS")
                return std::chrono::duration < int, std::ratio < 60 * 60 * 24 >> (duration * 60 *
                                                                                  60 * 24);
            return std::chrono::duration < int, std::ratio < 1 >> (1);
        }

        class UrlShortener final : public userver::server::handlers::HttpHandlerBase {
        public:
            static constexpr std::string_view
            kName = "handler-v1-make-shorter";

            UrlShortener(const userver::components::ComponentConfig &config,
                         const userver::components::ComponentContext &component_context)
                    : HttpHandlerBase(config, component_context),
                      pg_cluster_(
                              component_context
                                      .FindComponent<userver::components::Postgres>("postgres-db-1")
                                      .GetCluster()) {}

            std::string HandleRequestThrow(
                    const userver::server::http::HttpRequest &request,
                    userver::server::request::RequestContext &) const override {
                auto request_body =
                        userver::formats::json::FromString(request.RequestBody());

                auto url = request_body["url"].As < std::optional < std::string >> ();
                auto vip_key = request_body["vip_key"].As < std::optional < std::string >> ();
                auto time_to_live = request_body["time_to_live"].As < std::optional < int >> ();
                auto time_to_live_unit =
                        request_body["time_to_live_unit"].As < std::optional < std::string >> ();

                if (!url.has_value()) {
                    auto &response = request.GetHttpResponse();
                    response.SetStatus(userver::server::http::HttpStatus::kBadRequest);
                    return {};
                }

                if (!time_to_live_unit.has_value()) {
                    time_to_live_unit = "HOURS";
                }

                if (time_to_live.has_value()) {
                    bool correct_time_to_live = true;
                    std::string time_units[] = {"SECONDS", "MINUTES", "HOURS", "DAYS"};

                    if (time_to_live < 0)
                        correct_time_to_live = false;
                    else if (time_to_live > 2 && time_to_live_unit == time_units[3])
                        correct_time_to_live = false;
                    else if (time_to_live > 48 && time_to_live_unit == time_units[2])
                        correct_time_to_live = false;
                    else if (time_to_live > 2880 && time_to_live_unit == time_units[1])
                        correct_time_to_live = false;
                    else if (time_to_live > 172800 && time_to_live_unit == time_units[0])
                        correct_time_to_live = false;
                    else if (std::find(std::begin(time_units), std::end(time_units),
                                       time_to_live_unit) == std::end(time_units))
                        correct_time_to_live = false;

                    if (!correct_time_to_live) {
                        auto &response = request.GetHttpResponse();
                        response.SetStatus(userver::server::http::HttpStatus::kBadRequest);
                        return {};
                    }
                } else {
                    time_to_live = 10;
                    time_to_live_unit = "HOURS";
                }

                if (vip_key.has_value()) {
                    auto delete_incorrect_data = pg_cluster_->Execute(  // our id
                            userver::storages::postgres::ClusterHostType::kMaster,
                            "DELETE FROM url_shortener.urls WHERE id = $1 AND expiry_time < "
                            "$2::TIMESTAMPTZ",
                            vip_key.value(), userver::utils::datetime::Now());

                    auto valid_url = pg_cluster_->Execute(  // our id
                            userver::storages::postgres::ClusterHostType::kMaster,
                            "SELECT url FROM url_shortener.urls WHERE id = $1", vip_key.value());

                    if (!valid_url.IsEmpty()) {
                        auto &response = request.GetHttpResponse();
                        response.SetStatus(userver::server::http::HttpStatus::kBadRequest);
                        return {};
                    } else {
                        userver::formats::json::ValueBuilder response;
                        if (time_to_live == 0) {
                            response["short_url"] =
                                    fmt::format("http://localhost:8080/{}", vip_key.value());
                        } else {
                            auto duration_of_life =
                                    correct_time(time_to_live_unit.value(), time_to_live.value());
                            auto result = pg_cluster_->Execute(
                                    userver::storages::postgres::ClusterHostType::kMaster,
                                    "WITH ins AS ( "
                                    "INSERT INTO url_shortener.urls(id, url, expiry_time) VALUES($1, "
                                    "$2, $3) "
                                    "ON CONFLICT DO NOTHING "
                                    "RETURNING urls.id "
                                    ") "
                                    "SELECT id FROM url_shortener.urls WHERE url = $1 "
                                    "UNION ALL "
                                    "SELECT id FROM ins",
                                    vip_key.value(), url.value(),
                                    userver::utils::datetime::Now() + duration_of_life);

                            response["short_url"] = fmt::format(
                                    "http://localhost:8080/{}", result.AsSingleRow<std::string>());
                        }
                        return userver::formats::json::ToString(response.ExtractValue());
                    }
                } else {
                    auto result = pg_cluster_->Execute(
                            userver::storages::postgres::ClusterHostType::kMaster,
                            "WITH ins AS ( "
                            "INSERT INTO url_shortener.urls(url) VALUES($1) "
                            "ON CONFLICT DO NOTHING "
                            "RETURNING urls.id "
                            ") "
                            "SELECT id FROM url_shortener.urls WHERE url = $1 "
                            "UNION ALL "
                            "SELECT id FROM ins",
                            url.value());
                    userver::formats::json::ValueBuilder response;
                    response["short_url"] = fmt::format("http://localhost:8080/{}",
                                                        result.AsSingleRow<std::string>());

                    return userver::formats::json::ToString(response.ExtractValue());
                }
            }

            userver::storages::postgres::ClusterPtr pg_cluster_;
        };
    }  // namespace

    void AppendUrlShortener(userver::components::ComponentList &component_list) {
        component_list.Append<UrlShortener>();
    }
}  // namespace url_shortener