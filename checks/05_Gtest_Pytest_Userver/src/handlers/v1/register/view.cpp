#include "view.hpp"

#include <fmt/format.h>

#include <userver/crypto/hash.hpp>
#include <userver/server/handlers/http_handler_base.hpp>
#include <userver/server/http/http_status.hpp>
#include <userver/storages/postgres/cluster.hpp>
#include <userver/storages/postgres/component.hpp>
#include <userver/utils/assert.hpp>

#include "../../../models/bookmark.hpp"

namespace bookmarker {

namespace {

class RegisterUser final : public userver::server::handlers::HttpHandlerBase {
public:
    static constexpr std::string_view kName = "handler-register-user";

    RegisterUser(const userver::components::ComponentConfig& config,
                 const userver::components::ComponentContext& component_context)
        : HttpHandlerBase(config, component_context),
            pg_cluster_(
                component_context
                    .FindComponent<userver::components::Postgres>("postgres-db-1")
                    .GetCluster()) {}

    std::string HandleRequestThrow(
        const userver::server::http::HttpRequest& request,
        userver::server::request::RequestContext&
    ) const override {
        auto email = request.GetFormDataArg("email").value;
        auto password_raw = request.GetFormDataArg("password").value;

        if(email.empty()){
            auto& response = request.GetHttpResponse();
            response.SetStatus(userver::server::http::HttpStatus::kBadRequest);
            userver::formats::json::ValueBuilder response_text;
            response_text["message"] = "No email value for registration!";
            response_text["code"] = 400;
            return userver::formats::json::ToString(response_text.ExtractValue());
        }

        if(password_raw.empty()){
            auto& response = request.GetHttpResponse();
            response.SetStatus(userver::server::http::HttpStatus::kBadRequest);
            userver::formats::json::ValueBuilder response_text;
            response_text["message"] = "No password value for registration!";
            response_text["code"] = 400;
            return userver::formats::json::ToString(response_text.ExtractValue());
        }

        auto password = userver::crypto::hash::Sha256(password_raw);

        auto result = pg_cluster_->Execute(
            userver::storages::postgres::ClusterHostType::kMaster,
            "WITH ins AS ( "
            "INSERT INTO bookmarker.users(email, password) VALUES($1, $2) "
            "ON CONFLICT DO NOTHING "
            "RETURNING users.id"
            ") "
            "SELECT id FROM bookmarker.users WHERE email = $1 AND password = $2 "
            "UNION ALL "
            "SELECT id FROM ins; ",
            email, password
        );

        if(result.IsEmpty()){
            auto& response = request.GetHttpResponse();
            response.SetStatus(userver::server::http::HttpStatus::kConflict);
            userver::formats::json::ValueBuilder response_text;
            response_text["message"] = "Email with different password already registered!";
            response_text["code"] = 409;
            return userver::formats::json::ToString(response_text.ExtractValue());
        }

        userver::formats::json::ValueBuilder response_text;
        response_text["id"] = result.AsSingleRow<std::string>();

        request.GetHttpResponse().SetStatus(userver::server::http::HttpStatus::kCreated);
        return userver::formats::json::ToString(response_text.ExtractValue());
    }

private:
    userver::storages::postgres::ClusterPtr pg_cluster_;
};

}  // namespace

void AppendRegisterUser(userver::components::ComponentList& component_list) {
    component_list.Append<RegisterUser>();
}

}  // namespace bookmarker
