#include "view.hpp"

#include <fmt/format.h>

#include <userver/server/handlers/http_handler_base.hpp>
#include <userver/server/http/http_status.hpp>
#include <userver/storages/postgres/cluster.hpp>
#include <userver/storages/postgres/component.hpp>
#include <userver/utils/assert.hpp>
#include <userver/crypto/hash.hpp>

#include "../../../models/user.hpp"

namespace bookmarker {

namespace {

class LoginUser final : public userver::server::handlers::HttpHandlerBase {
public:
    static constexpr std::string_view kName = "handler-login-user";

    LoginUser(const userver::components::ComponentConfig& config,
              const userver::components::ComponentContext& component_context)
        : HttpHandlerBase(config, component_context),
            pg_cluster_(
                component_context
                    .FindComponent<userver::components::Postgres>("postgres-db-1")
                    .GetCluster()) {}

    std::string HandleRequestThrow(
        const userver::server::http::HttpRequest& request,
        userver::server::request::RequestContext&
    ) const override {
        auto email = request.GetFormDataArg("email").value;
        auto password_raw = request.GetFormDataArg("password").value;

        if(email.empty()){
            auto& response = request.GetHttpResponse();
            response.SetStatus(userver::server::http::HttpStatus::kBadRequest);
            userver::formats::json::ValueBuilder response_text;
            response_text["message"] = "No email value for registration!";
            response_text["code"] = 400;
            return userver::formats::json::ToString(response_text.ExtractValue());
        }

        if(password_raw.empty()){
            auto& response = request.GetHttpResponse();
            response.SetStatus(userver::server::http::HttpStatus::kBadRequest);
            userver::formats::json::ValueBuilder response_text;
            response_text["message"] = "No password value for registration!";
            response_text["code"] = 400;
            return userver::formats::json::ToString(response_text.ExtractValue());
        }

        auto password = userver::crypto::hash::Sha256(password_raw);

        auto userResult = pg_cluster_->Execute(
            userver::storages::postgres::ClusterHostType::kMaster,
            "SELECT * FROM bookmarker.users "
            "WHERE email = $1 ",
            email
        );

        if (userResult.IsEmpty()) {
            auto& response = request.GetHttpResponse();
            response.SetStatus(userver::server::http::HttpStatus::kNotFound);
            return {};
        }

        auto user = userResult.AsSingleRow<TUser>(userver::storages::postgres::kRowTag);
        if (password != user.password) {
            auto& response = request.GetHttpResponse();
            response.SetStatus(userver::server::http::HttpStatus::kNotFound);
            return {};
        }

        auto result = pg_cluster_->Execute(
            userver::storages::postgres::ClusterHostType::kMaster,
            "WITH ins AS ( "
            "INSERT INTO bookmarker.auth_sessions(user_id) VALUES($1) "
            "ON CONFLICT DO NOTHING "
            "RETURNING auth_sessions.id"
            ") "
            "SELECT id FROM bookmarker.auth_sessions WHERE user_id = $1 "
            "UNION ALL "
            "SELECT id FROM ins; ",
            user.id
        );

        userver::formats::json::ValueBuilder response;
        response["id"] = result.AsSingleRow<std::string>();

        request.GetHttpResponse().SetStatus(userver::server::http::HttpStatus::kCreated);
        return userver::formats::json::ToString(response.ExtractValue());
    }

private:
    userver::storages::postgres::ClusterPtr pg_cluster_;
};

}  // namespace

void AppendLoginUser(userver::components::ComponentList& component_list) {
    component_list.Append<LoginUser>();
}

}  // namespace bookmarker
