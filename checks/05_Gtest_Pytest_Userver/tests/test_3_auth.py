import pytest
import json

json = {
    "url": "https://yandex.ru",
    "title": "Yandex",
    "tag": "string"
}


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_add_bookmark_no_header(service_client):
    headers = {
        'Content-Type': 'application/json'
    }
    response = await service_client.post(
        '/v1/bookmarks',
        json=json,
        headers=headers
    )

    assert response.status == 401


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_add_bookmark_wrong_header(service_client):
    headers_wrong = {
        'Content-Type': 'application/json',
        'X-Ya-User-Ticket': "empty"
    }

    response = await service_client.post(
        '/v1/bookmarks',
        json=json,
        headers=headers_wrong
    )

    assert response.status == 401


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_add_bookmark_ok(service_client):
    headers_ok = {
        'Content-Type': 'application/json',
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }
    response = await service_client.post(
        '/v1/bookmarks',
        json=json,
        headers=headers_ok
    )

    assert response.status == 201
