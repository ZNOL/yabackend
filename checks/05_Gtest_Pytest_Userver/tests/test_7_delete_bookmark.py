import pytest


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_delete_bookmark_empty_auth(service_client):
    headers2 = {}

    response_add = await service_client.delete(
        '/v1/bookmarks/61f7e684-0226-46cc-a28a-c012a912ef2f',
        headers=headers2
    )

    assert response_add.status == 401


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_delete_bookmark_wrong_ticket(service_client):
    headers2 = {
        'X-Ya-User-Ticket': '61f7e684-0226-46cc-a28a-c012a912ef2e'
    }

    response_add = await service_client.delete(
        '/v1/bookmarks/61f7e684-0226-46cc-a28a-c012a912ef2f',
        headers=headers2
    )

    assert response_add.status == 401


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_delete_bookmark_wrong_id(service_client):
    headers2 = {
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response_add = await service_client.delete(
        '/v1/bookmarks/61f7e684-0226-46cc-a28a-c012a912ef2f',
        headers=headers2
    )

    assert response_add.status == 404


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_delete_bookmark_wrong_user_parent(service_client):
    headers2 = {
        'Content-Type': 'application/json',
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea58'
    }

    response_add = await service_client.delete(
        '/v1/bookmarks/7cab5af3-9330-4d0c-a8f5-030f2b5ee877',
        headers=headers2
    )

    assert response_add.status == 404


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_delete_bookmark_full(service_client):
    headers2 = {
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response_add = await service_client.delete(
        '/v1/bookmarks/7cab5af3-9330-4d0c-a8f5-030f2b5ee877',
        headers=headers2
    )

    assert response_add.status == 200
