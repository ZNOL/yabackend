import pytest
import json


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_get_bookmarks_empty_auth(service_client):
    headers2 = {}

    response = await service_client.get(
        '/v1/bookmarks',
        headers=headers2
    )

    assert response.status == 401


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_get_bookmarks_wrong_ticket(service_client):
    headers2 = {
        'X-Ya-User-Ticket': '61f7e684-0226-46cc-a28a-c012a912ef2e'
    }

    response = await service_client.get(
        '/v1/bookmarks',
        params={},
        headers=headers2
    )

    assert response.status == 401


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_get_bookmarks_wrong_limit(service_client):
    headers2 = {
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response = await service_client.get(
        '/v1/bookmarks',
        params={
            'limit': 'a'
        },
        headers=headers2
    )

    assert response.status == 400


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_get_bookmarks_wrong_order(service_client):
    headers2 = {
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response = await service_client.get(
        '/v1/bookmarks',
        params={
            'order_by': 'kapysta'
        },
        headers=headers2
    )

    assert response.status == 400


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_get_bookmarks_only_limit(service_client):
    headers2 = {
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response = await service_client.get(
        '/v1/bookmarks',
        params={
            'limit': '5'
        },
        headers=headers2
    )

    assert response.status == 200
    assert json.loads(response.content) == \
        {
            'items': [
                    {
                        'created_ts': '2022-10-26T11:09:02.928308+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee877',
                        'title': 'Yandex',
                        'url': 'yandex2.ru'
                    },
                    {
                        'created_ts': '2022-10-26T11:09:02.928309+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee878',
                        'title': 'Yandex',
                        'url': 'yandex1.ru'
                    },
                    {
                        'created_ts': '2022-10-26T11:09:02.928306+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee879',
                        'title': 'yandex',
                        'url': 'yandex3.ru'
                    }]
        }


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_get_bookmarks_only_order(service_client):
    headers2 = {
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response = await service_client.get(
        '/v1/bookmarks',
        params={
            'order_by': 'id'
        },
        headers=headers2
    )

    assert response.status == 200
    assert json.loads(response.content) == \
        {
            'items': [
                    {
                        'created_ts': '2022-10-26T11:09:02.928308+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee877',
                        'title': 'Yandex',
                        'url': 'yandex2.ru'
                    },
                    {
                        'created_ts': '2022-10-26T11:09:02.928309+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee878',
                        'title': 'Yandex',
                        'url': 'yandex1.ru'
                    },
                    {
                        'created_ts': '2022-10-26T11:09:02.928306+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee879',
                        'title': 'yandex',
                        'url': 'yandex3.ru'
                    }]
        }


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_get_bookmarks_only_tag(service_client):
    headers2 = {
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response = await service_client.get(
        '/v1/bookmarks',
        params={
            'tag': 'tag3'
        },
        headers=headers2
    )

    assert response.status == 200
    assert json.loads(response.content) == \
        {
            'items': [
                    {
                        'created_ts': '2022-10-26T11:09:02.928306+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee879',
                        'title': 'yandex',
                        'url': 'yandex3.ru'
                    }]
        }


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_get_bookmarks_order_id(service_client):
    headers2 = {
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response = await service_client.get(
        '/v1/bookmarks',
        params={
            'limit': '5',
            'order_by': 'id'
        },
        headers=headers2
    )

    assert response.status == 200
    assert json.loads(response.content) == \
        {
            'items': [
                    {
                        'created_ts': '2022-10-26T11:09:02.928308+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee877',
                        'title': 'Yandex',
                        'url': 'yandex2.ru'
                    },
                    {
                        'created_ts': '2022-10-26T11:09:02.928309+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee878',
                        'title': 'Yandex',
                        'url': 'yandex1.ru'
                    },
                    {
                        'created_ts': '2022-10-26T11:09:02.928306+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee879',
                        'title': 'yandex',
                        'url': 'yandex3.ru'
                    }]
        }


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_get_bookmarks_order_title(service_client):
    headers2 = {
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response = await service_client.get(
        '/v1/bookmarks',
        params={
            'limit': '5',
            'order_by': 'title'
        },
        headers=headers2
    )

    assert response.status == 200
    assert json.loads(response.content) == \
        {
            'items': [
                    {
                        'created_ts': '2022-10-26T11:09:02.928308+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee877',
                        'title': 'Yandex',
                        'url': 'yandex2.ru'
                    },
                    {
                        'created_ts': '2022-10-26T11:09:02.928309+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee878',
                        'title': 'Yandex',
                        'url': 'yandex1.ru'
                    },
                    {
                        'created_ts': '2022-10-26T11:09:02.928306+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee879',
                        'title': 'yandex',
                        'url': 'yandex3.ru'
                    }]
        }


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_get_bookmarks_order_url(service_client):
    headers2 = {
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response = await service_client.get(
        '/v1/bookmarks',
        params={
            'limit': '5',
            'order_by': 'url'
        },
        headers=headers2
    )

    assert response.status == 200
    assert json.loads(response.content) == \
        {
            'items': [
                    {
                        'created_ts': '2022-10-26T11:09:02.928309+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee878',
                        'title': 'Yandex',
                        'url': 'yandex1.ru'
                    },
                    {
                        'created_ts': '2022-10-26T11:09:02.928308+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee877',
                        'title': 'Yandex',
                        'url': 'yandex2.ru'
                    },
                    {
                        'created_ts': '2022-10-26T11:09:02.928306+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee879',
                        'title': 'yandex',
                        'url': 'yandex3.ru'
                    }]
        }


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_get_bookmarks_order_created_ts(service_client):
    headers2 = {
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response = await service_client.get(
        '/v1/bookmarks',
        params={
            'limit': '5',
            'order_by': 'created_ts'
        },
        headers=headers2
    )

    assert response.status == 200
    assert json.loads(response.content) == \
        {
            'items': [
                    {
                        'created_ts': '2022-10-26T11:09:02.928306+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee879',
                        'title': 'yandex',
                        'url': 'yandex3.ru'
                    },
                    {
                        'created_ts': '2022-10-26T11:09:02.928308+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee877',
                        'title': 'Yandex',
                        'url': 'yandex2.ru'
                    },
                    {
                        'created_ts': '2022-10-26T11:09:02.928309+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee878',
                        'title': 'Yandex',
                        'url': 'yandex1.ru'
                    }]
        }


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_get_bookmarks_full(service_client):
    headers2 = {
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response = await service_client.get(
        '/v1/bookmarks',
        params={
            'limit': '5',
            'order_by': 'url',
            'tag': 'tag1'
        },
        headers=headers2
    )

    assert response.status == 200
    assert json.loads(response.content) == \
        {
            'items': [
                    {
                        'created_ts': '2022-10-26T11:09:02.928308+00:00',
                        'id': '7cab5af3-9330-4d0c-a8f5-030f2b5ee877',
                        'title': 'Yandex',
                        'url': 'yandex2.ru'
                    }]
        }
