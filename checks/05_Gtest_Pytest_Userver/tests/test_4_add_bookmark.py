import aiohttp
import pytest
import json


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_add_bookmark_empty(service_client):
    headers2 = {
        'Content-Type': 'application/json',
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response_add = await service_client.post(
        '/v1/bookmarks',
        json={},
        headers=headers2
    )

    assert response_add.status == 400


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_add_bookmark_url_only(service_client):
    headers2 = {
        'Content-Type': 'application/json',
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response_add = await service_client.post(
        '/v1/bookmarks',
        json={
            "url": "https://yandex.ru"
        },
        headers=headers2
    )

    assert response_add.status == 400


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_add_bookmark_title_only(service_client):
    headers2 = {
        'Content-Type': 'application/json',
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response_add = await service_client.post(
        '/v1/bookmarks',
        json={
            "title": "https://yandex.ru"
        },
        headers=headers2
    )

    assert response_add.status == 400


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_add_bookmark_title_and_url(service_client):
    headers2 = {
        'Content-Type': 'application/json',
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response_add = await service_client.post(
        '/v1/bookmarks',
        json={
            "url": "https://yandex.ru",
            "title": "Yandex"
        },
        headers=headers2
    )

    assert response_add.status == 201


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
async def test_add_bookmark_full(service_client):
    headers2 = {
        'Content-Type': 'application/json',
        'X-Ya-User-Ticket': 'd078ae06-76a0-4f1e-bd1b-18913dc1ea57'
    }

    response_add = await service_client.post(
        '/v1/bookmarks',
        json={
            "url": "https://yandex.ru",
            "title": "Yandex",
            "tag": "string"
        },
        headers=headers2
    )

    assert response_add.status == 201
