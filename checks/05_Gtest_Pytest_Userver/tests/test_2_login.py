import aiohttp
import pytest
import json

async def test_login_empty(service_client):
    with aiohttp.MultipartWriter('form-data') as data:
        payload_pswd = aiohttp.payload.StringPayload('empty')
        payload_pswd.set_content_disposition('form-data', name='empty')
        data.append_payload(payload_pswd)

    headers = {
        'Content-Type': 'multipart/form-data; boundary=' + data.boundary,
    }

    response = await service_client.post(
        '/login',
        data=data,
        headers=headers
    )

    assert response.status == 400
    assert json.loads(response.content)["message"] == \
        "No email value for registration!"

async def test_login_no_password(service_client):
    with aiohttp.MultipartWriter('form-data') as data:
        payload_email = aiohttp.payload.StringPayload('m2@yandex.ru')
        payload_email.set_content_disposition('form-data', name='email')
        data.append_payload(payload_email)

    headers = {
        'Content-Type': 'multipart/form-data; boundary=' + data.boundary,
    }

    response = await service_client.post(
        '/login',
        data=data,
        headers=headers
    )

    assert response.status == 400
    assert json.loads(response.content)["message"] == \
        "No password value for registration!"

async def test_login_no_email(service_client):
    with aiohttp.MultipartWriter('form-data') as data:
        payload_pswd = aiohttp.payload.StringPayload('pswd')
        payload_pswd.set_content_disposition('form-data', name='password')
        data.append_payload(payload_pswd)

    headers = {
        'Content-Type': 'multipart/form-data; boundary=' + data.boundary,
    }

    response = await service_client.post(
        '/login',
        data=data,
        headers=headers
    )

    assert response.status == 400
    assert json.loads(response.content)["message"] == \
        "No email value for registration!"

async def test_login_not_valid(service_client):
    with aiohttp.MultipartWriter('form-data') as data:
        payload_email = aiohttp.payload.StringPayload('m2@yandex.ru')
        payload_email.set_content_disposition('form-data', name='email')
        payload_pswd = aiohttp.payload.StringPayload('pswd')
        payload_pswd.set_content_disposition('form-data', name='password')
        data.append_payload(payload_email)
        data.append_payload(payload_pswd)

    headers = {
        'Content-Type': 'multipart/form-data; boundary=' + data.boundary,
    }

    response = await service_client.post(
        '/login',
        data=data,
        headers=headers
    )

    assert response.status == 404

async def test_login_different_login(service_client):
    with aiohttp.MultipartWriter('form-data') as data:
        payload_email = aiohttp.payload.StringPayload('m2@yandex.ru')
        payload_email.set_content_disposition('form-data', name='email')
        payload_pswd = aiohttp.payload.StringPayload('pswd')
        payload_pswd.set_content_disposition('form-data', name='password')
        data.append_payload(payload_email)
        data.append_payload(payload_pswd)

    headers = {
        'Content-Type': 'multipart/form-data; boundary=' + data.boundary,
    }

    response = await service_client.post(
        '/register',
        data=data,
        headers=headers
    )

    with aiohttp.MultipartWriter('form-data') as data2:
        payload_email = aiohttp.payload.StringPayload('m3@yandex.ru')
        payload_email.set_content_disposition('form-data', name='email')
        payload_pswd = aiohttp.payload.StringPayload('pswd')
        payload_pswd.set_content_disposition('form-data', name='password')
        data2.append_payload(payload_email)
        data2.append_payload(payload_pswd)

    headers2 = {
        'Content-Type': 'multipart/form-data; boundary=' + data2.boundary,
    }

    response = await service_client.post(
        '/login',
        data=data2,
        headers=headers2
    )

    assert response.status == 404


async def test_login_different_password(service_client):
    with aiohttp.MultipartWriter('form-data') as data:
        payload_email = aiohttp.payload.StringPayload('m2@yandex.ru')
        payload_email.set_content_disposition('form-data', name='email')
        payload_pswd = aiohttp.payload.StringPayload('pswd')
        payload_pswd.set_content_disposition('form-data', name='password')
        data.append_payload(payload_email)
        data.append_payload(payload_pswd)

    headers = {
        'Content-Type': 'multipart/form-data; boundary=' + data.boundary,
    }

    response = await service_client.post(
        '/register',
        data=data,
        headers=headers
    )

    with aiohttp.MultipartWriter('form-data') as data2:
        payload_email = aiohttp.payload.StringPayload('m2@yandex.ru')
        payload_email.set_content_disposition('form-data', name='email')
        payload_pswd = aiohttp.payload.StringPayload('pswd1')
        payload_pswd.set_content_disposition('form-data', name='password')
        data2.append_payload(payload_email)
        data2.append_payload(payload_pswd)

    headers2 = {
        'Content-Type': 'multipart/form-data; boundary=' + data2.boundary,
    }

    response = await service_client.post(
        '/login',
        data=data2,
        headers=headers2
    )

    assert response.status == 404

async def test_login_double(service_client):
    with aiohttp.MultipartWriter('form-data') as data:
        payload_email = aiohttp.payload.StringPayload('m2@yandex.ru')
        payload_email.set_content_disposition('form-data', name='email')
        payload_pswd = aiohttp.payload.StringPayload('pswd')
        payload_pswd.set_content_disposition('form-data', name='password')
        data.append_payload(payload_email)
        data.append_payload(payload_pswd)

    headers = {
        'Content-Type': 'multipart/form-data; boundary=' + data.boundary,
    }
    response = await service_client.post(
        '/register',
        data=data,
        headers=headers
    )

    response = await service_client.post(
        '/login',
        data=data,
        headers=headers
    )

    assert response.status == 201

    response = await service_client.post(
        '/login',
        data=data,
        headers=headers
    )

    assert response.status == 201
