INSERT INTO bookmarker.users(id,email, password) VALUES
    ('61f7e684-0226-46cc-a28a-c012a912ef2f','m2@yandex.ru', 'pswd'),
    ('61f7e684-0226-46cc-a28a-c012a912ef2g','m3@yandex.ru', 'pswd')
ON CONFLICT(email) DO NOTHING;

INSERT INTO bookmarker.auth_sessions(id,user_id) VALUES
    ('d078ae06-76a0-4f1e-bd1b-18913dc1ea57','61f7e684-0226-46cc-a28a-c012a912ef2f'),
    ('d078ae06-76a0-4f1e-bd1b-18913dc1ea58','61f7e684-0226-46cc-a28a-c012a912ef2g')
ON CONFLICT DO NOTHING;

INSERT INTO bookmarker.bookmarks(id, owner_id, url, title, tag, created_ts) VALUES
    ('7cab5af3-9330-4d0c-a8f5-030f2b5ee877','61f7e684-0226-46cc-a28a-c012a912ef2f', 'yandex2.ru', 'Yandex', 'tag1', '2022-10-26T11:09:02.928308+00:00'),
    ('7cab5af3-9330-4d0c-a8f5-030f2b5ee878','61f7e684-0226-46cc-a28a-c012a912ef2f', 'yandex1.ru', 'Yandex', 'tag2', '2022-10-26T11:09:02.928309+00:00'),
    ('7cab5af3-9330-4d0c-a8f5-030f2b5ee879','61f7e684-0226-46cc-a28a-c012a912ef2f', 'yandex3.ru', 'yandex', 'tag3', '2022-10-26T11:09:02.928306+00:00')
ON CONFLICT DO NOTHING;