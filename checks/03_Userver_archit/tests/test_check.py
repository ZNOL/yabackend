import pytest


@pytest.mark.parametrize(
    "params",
    [
        {},
        {'user_id': None},
        {'user_id': "foo"},
        {'user_id': "1_"},
        {'user_id': ""},
        {'user_id': 42},
        {'user_id': 1, 'item_id': []},
        {'user_id': 1, 'item_id': ['foo']},
        {'user_id': 1, 'item_id': ['foo*123']},
        {'user_id': 1, 'item_id': ['___']},
        {'user_id': 1, 'item_id': ['']},
    ],
)
async def test_bad_request(service_client, params):
    response = await service_client.get(
        '/check',
        params=params,
    )
    assert response.status == 400
    assert response.json()['message'] == 'Bad Request'


@pytest.mark.pgsql('db-1', files=['initial_data.sql'])
@pytest.mark.parametrize(
    "params, responses",
    [
        (
            {'user_id': 1, 'item_id': ['category_123']},
            [{"item_id": "", "problem": "WRONG_CATEGORY"}],
        ),
        (
            {'user_id': 1, 'item_id': ['category_querty']},
            [{"item_id": "", "problem": "WRONG_CATEGORY"}],
        ),
        (
            {'user_id': 1, 'item_id': ['common_querty']},
            [{"item_id": "", "problem": "INCORRECT_ITEM_ID"}],
        ),
        (
            {'user_id': 1, 'item_id': ['coMMon_querty']},
            [{"item_id": "", "problem": "INCORRECT_ITEM_ID"}],
        ),
        (
            {'user_id': 1, 'item_id': ['COMMON_querty']},
            [{"item_id": "", "problem": "INCORRECT_ITEM_ID"}],
        ),
        (
            {'user_id': 1, 'item_id': ['receipt_querty']},
            [{"item_id": "", "problem": "INCORRECT_ITEM_ID"}],
        ),
        (
            {'user_id': 1, 'item_id': ['special_querty']},
            [{"item_id": "", "problem": "INCORRECT_ITEM_ID"}],
        ),
        (
            {'user_id': 1, 'item_id': ['special_1000']},
            [{"item_id": "special_1000", "problem": "ITEM_NOT_FOUND"}],
        ),
        (
            {'user_id': 61, 'item_id': ['special_1000']},
            [{"item_id": "special_1000", "problem": "ITEM_NOT_FOUND"}],
        ),
        (
            {'user_id': 1000, 'item_id': ['common_1']},
            [{"item_id": 'common_1', "problem": "NO_USER"}],
        ),
        (
            {'user_id': 1000, 'item_id': ['receipt_1']},
            [{"item_id": 'receipt_1', "problem": "NO_USER_NO_RECEIPT"}],
        ),
        (
            {'user_id': 1000, 'item_id': ['special_1']},
            [{"item_id": 'special_1', "problem": "NO_USER_SPECIAL_ITEM"}],
        ),
        (
            {'user_id': 1, 'item_id': ['special_1']},
            [{"item_id": 'special_1', "problem": "ITEM_IS_SPECIAL"}],
        ),
        (
            {'user_id': 1, 'item_id': ['special_1']},
            [{"item_id": 'special_1', "problem": "ITEM_IS_SPECIAL"}],
        ),
        (
            {'user_id': 61, 'item_id': ['special_9']},
            [{"item_id": 'special_9', "problem": "ITEM_SPECIAL_WRONG_SPECIFIC"}],
        ),
        (
            {'user_id': 1, 'item_id': ['receipt_9']},
            [{"item_id": 'receipt_9', "problem": "NO_RECEIPT"}],
        ),
        (
            {'user_id': 1, 'item_id': ['common_1']},
            [],
        ),
        (
            {'user_id': 61, 'item_id': ['common_1']},
            [],
        ),
        (
            {'user_id': 61, 'item_id': ['receipt_1']},
            [],
        ),
    ],
)
async def test_violations(service_client, params, responses):
    response = await service_client.get('/check', params=params)
    assert response.status == 200
    assert response.json() == responses
