#pragma once

#include "optional"
#include "string"
#include "string_view"
#include "vector"

namespace code_architecture {

std::string_view Strip(std::string_view s);

std::vector<std::string_view> SplitBy(std::string_view s,
                                      std::string_view delimiter);

}  // namespace code_architecture