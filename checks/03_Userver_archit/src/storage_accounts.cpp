#include "storage_accounts.hpp"

namespace code_architecture::storage {

Accounts::Accounts(
    const userver::components::ComponentConfig& config,
    const userver::components::ComponentContext& component_context)
    : LoggableComponentBase(config, component_context),
      pg_cluster_(
          component_context
              .FindComponent<userver::components::Postgres>("postgres-db-1")
              .GetCluster()) {}

Accounts::~Accounts() = default;

models::Account Accounts::GetAccount(int64_t user_id) const {
  auto customer = FindCustomerByID(user_id);
  if (customer.has_value()) {
    return customer.value();
  }

  auto doctor = FindDoctorByID(user_id);
  if (doctor.has_value()) {
    return doctor.value();
  }

  return std::monostate{};
}

std::optional<models::users::Customer> Accounts::FindCustomerByID(
    int64_t user_id) const {
  auto result = pg_cluster_->Execute(
      userver::storages::postgres::ClusterHostType::kMaster,
      sql::kSelectUserAccount, user_id);
  if (result.IsEmpty()) {
    return std::nullopt;
  }

  models::users::Customer customer{
      /*.id_=*/result[0]["id"].As<int64_t>(),
      /*.full_name_=*/result[0]["full_name"].As<std::string>(),
      /*.phone_=*/result[0]["phone"].As<std::string>(),

      /*.recipes_=*/
      pg_cluster_
          ->Execute(userver::storages::postgres::ClusterHostType::kMaster,
                    sql::kSelectCustomerReceipts, customer.id_)
          .AsContainer<std::unordered_set<int64_t>>(),
  };

  return customer;
}

std::optional<models::users::Doctor> Accounts::FindDoctorByID(
    int64_t user_id) const {
  auto result = pg_cluster_->Execute(
      userver::storages::postgres::ClusterHostType::kMaster,
      sql::kSelectDoctorAccount, user_id);
  if (!result.IsEmpty()) {
    return result.AsSingleRow<models::users::Doctor>(
        userver::storages::postgres::kRowTag);
  }

  return std::nullopt;
}

void AppendAccounts(userver::components::ComponentList& component_list) {
  component_list.Append<Accounts>();
  component_list.Append<userver::components::Postgres>("postgres-db-1");
  component_list.Append<userver::clients::dns::Component>();
}

}  // namespace code_architecture::storage