#pragma once

#include <memory>
#include <unordered_map>

#include <boost/algorithm/string.hpp>
#include <boost/lexical_cast.hpp>

#include <userver/formats/json/value.hpp>
#include <userver/formats/parse/common_containers.hpp>
#include <userver/server/handlers/http_handler_json_base.hpp>

#include "parse.hpp"

namespace code_architecture::models {

namespace users {

struct Customer {
  int64_t id_ = 0;
  std::string full_name_;
  std::string phone_;
  std::unordered_set<int64_t> recipes_;
};

struct Doctor {
  int64_t id_ = 0;
  std::string full_name_;
  std::string phone_;
  std::string speciality_;
};

}  // namespace users

using NoAccount = std::monostate;
using Account =
    std::variant<NoAccount, models::users::Customer, models::users::Doctor>;

namespace items {

struct Common {
  int64_t id_ = 0;
  std::string name_;
};

struct Recipe {
  int64_t id_ = 0;
  std::string name_;
};

struct Special {
  int64_t id_ = 0;
  std::string name_;
  std::string speciality_;
};

}  // namespace items

using NoItem = std::monostate;
using Medicament = std::variant<NoItem, models::items::Common,
                                models::items::Recipe, models::items::Special>;
namespace cart {

enum class Category {
  COMMON,
  RECIPE,
  SPECIAL,
  UNKNOWN,
};

struct Item {
  explicit Item(const std::string& data);
  std::optional<int64_t> id_;
  Category category_ = Category::UNKNOWN;
  std::string raw_id_;
};

enum class ViolationType {
  WRONG_CATEGORY,
  INCORRECT_ITEM_ID,
  ITEM_NOT_FOUND,
  NO_USER,
  NO_USER_NO_RECEIPT,
  NO_USER_SPECIAL_ITEM,
  NO_RECEIPT,
  ITEM_IS_SPECIAL,
  ITEM_SPECIAL_WRONG_SPECIFIC,
  CORRECT,
};

std::string ViolationTypeToString(ViolationType violation_type);

struct Violation {
  std::shared_ptr<Item> item_;
  ViolationType problem_ = ViolationType::CORRECT;
};

class Cart {
 public:
  Cart() = default;

  void SetUserID(const std::string& user_id);
  void SetItems(const std::vector<std::string>& items);
  int64_t GetUserID() const;
  const std::vector<Item>& GetItems() const;
  const std::vector<Violation>& GetViolations() const;
  void AddViolation(std::shared_ptr<Item> item, ViolationType type);
  void AddViolation(Violation&& violation);

 private:
  int64_t user_id_ = 0;
  std::vector<Item> items_;
  std::vector<Violation> violations_;
};

Category FromString(std::string data);

}  // namespace cart

}  // namespace code_architecture::models