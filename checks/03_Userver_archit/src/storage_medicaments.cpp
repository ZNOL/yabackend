#include "storage_medicaments.hpp"

namespace code_architecture::storage {

Medicaments::Medicaments(
    const userver::components::ComponentConfig& config,
    const userver::components::ComponentContext& component_context)
    : LoggableComponentBase(config, component_context),
      pg_cluster_(
          component_context
              .FindComponent<userver::components::Postgres>("postgres-db-1")
              .GetCluster()) {}

Medicaments::~Medicaments() = default;

models::Medicament Medicaments::GetMedicament(
    int64_t item_id, models::cart::Category category) const {
  switch (category) {
    case models::cart::Category::COMMON: {
      auto item = FindCommonByID(item_id);
      if (item.has_value()) {
        return item.value();
      }
      break;
    }
    case models::cart::Category::RECIPE: {
      auto item = FindRecipeByID(item_id);
      if (item.has_value()) {
        return item.value();
      }
      break;
    }
    case models::cart::Category::SPECIAL: {
      auto item = FindSpecialByID(item_id);
      if (item.has_value()) {
        return item.value();
      }
      break;
    }
    case models::cart::Category::UNKNOWN:
      return models::NoItem{};
  }

  return models::NoItem{};
}

std::optional<models::items::Common> Medicaments::FindCommonByID(
    int64_t item_id) const {
  auto result = pg_cluster_->Execute(
      userver::storages::postgres::ClusterHostType::kMaster,
      sql::kSelectCommonItem, item_id);
  if (!result.IsEmpty()) {
    return result.AsSingleRow<models::items::Common>(
        userver::storages::postgres::kRowTag);
  }

  return std::nullopt;
}

std::optional<models::items::Recipe> Medicaments::FindRecipeByID(
    int64_t item_id) const {
  auto result = pg_cluster_->Execute(
      userver::storages::postgres::ClusterHostType::kMaster,
      sql::kSelectRecipeItem, item_id);
  if (!result.IsEmpty()) {
    return result.AsSingleRow<models::items::Recipe>(
        userver::storages::postgres::kRowTag);
  }

  return std::nullopt;
}

std::optional<models::items::Special> Medicaments::FindSpecialByID(
    int64_t item_id) const {
  auto result = pg_cluster_->Execute(
      userver::storages::postgres::ClusterHostType::kMaster,
      sql::kSelectSpecialItem, item_id);
  if (!result.IsEmpty()) {
    return result.AsSingleRow<models::items::Special>(
        userver::storages::postgres::kRowTag);
  }

  return std::nullopt;
}

}  // namespace code_architecture::storage
