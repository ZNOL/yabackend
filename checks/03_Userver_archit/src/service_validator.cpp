//
// Created by chistopat on 10/17/22.
//

#include "service_validator.hpp"

namespace code_architecture::service {

models::cart::ViolationType Validate(const models::Account& account,
                                     const models::Medicament& medicament) {
  return std::visit(Checker, account, medicament);
}

}  // namespace code_architecture::service
