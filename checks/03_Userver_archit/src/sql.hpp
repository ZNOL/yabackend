#pragma once

#include <string>

namespace code_architecture::storage::sql {

const std::string kSelectUserAccount = R"~(
SELECT id, full_name, phone
FROM code_architecture.user_account
WHERE id = $1
)~";

const std::string kSelectDoctorAccount = R"~(
SELECT
   a.id as id,
   a.full_name as full_name,
   a.phone as phone,
   s.name as speciality
FROM code_architecture.doctor_account AS a
JOIN code_architecture.specialty AS s ON (a.specialty_id = s.id)
WHERE a.id = $1
)~";

const std::string kSelectCommonItem = R"~(
SELECT id, name
FROM code_architecture.common_item
WHERE id = $1
)~";

const std::string kSelectRecipeItem = R"~(
SELECT id, name
FROM code_architecture.receipt_item
WHERE id = $1
)~";

const std::string kSelectSpecialItem = R"~(
SELECT
  i.id as id,
  i.name as name,
  s.name as speciality
FROM code_architecture.special_item AS i
JOIN code_architecture.specialty AS s ON (i.specialty_id = s.id)
WHERE i.id = $1
)~";

const std::string kSelectCustomerReceipts = R"~(
SELECT item_id
FROM code_architecture.receipt
WHERE user_id = $1
)~";

}  // namespace code_architecture::storage::sql