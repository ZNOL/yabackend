#include "models.hpp"
#include <userver/logging/log.hpp>

namespace code_architecture::models {

namespace cart {

void Cart::SetUserID(const std::string& user_id) {
  if (user_id.empty()) {
    // user_id is required
    throw userver::server::handlers::ClientError();
  }

  try {
    user_id_ = boost::lexical_cast<int>(user_id);
  } catch (boost::bad_lexical_cast&) {
    // user_id must be correct integer
    // todo: custom error wrapper
    throw userver::server::handlers::ClientError();
  }
}

void Cart::SetItems(const std::vector<std::string>& items) {
  if (items.empty()) {
    // at least one item is required
    // todo: custom error wrapper
    throw userver::server::handlers::ClientError();
  }
  for (const auto& item : items) {
    items_.emplace_back(Item{item});
  }
}

int64_t Cart::GetUserID() const { return user_id_; }

const std::vector<Item>& Cart::GetItems() const { return items_; }

Category FromString(std::string data) {
  LOG_DEBUG() << "category_: " << data;

  boost::algorithm::to_lower(data);

  static const std::unordered_map<std::string, Category> kAllowed{
      {"common", Category::COMMON},
      {"receipt", Category::RECIPE},
      {"special", Category::SPECIAL},
  };

  if (kAllowed.count(data)) {
    return kAllowed.at(data);
  }

  return Category::UNKNOWN;
}

Item::Item(const std::string& data) {
  raw_id_ = data;

  auto tokens = SplitBy(data, "_");
  if (tokens.size() != 2) {
    // invalid item structure
    throw userver::server::handlers::RequestParseError();
  }

  category_ = FromString(std::string(tokens[0]));

  try {
    id_ = boost::lexical_cast<int64_t>(tokens[1].data());
  } catch (boost::bad_lexical_cast&) {
    id_ = std::nullopt;
  }
}

const std::vector<Violation>& Cart::GetViolations() const {
  return violations_;
}

void Cart::AddViolation(std::shared_ptr<Item> item, ViolationType type) {
  violations_.emplace_back(Violation{item, type});
}

void Cart::AddViolation(Violation&& violation) {
  violations_.push_back(violation);
}

std::string ViolationTypeToString(ViolationType violation_type) {
  switch (violation_type) {
    case ViolationType::WRONG_CATEGORY:
      return "WRONG_CATEGORY";
    case ViolationType::INCORRECT_ITEM_ID:
      return "INCORRECT_ITEM_ID";
    case ViolationType::ITEM_NOT_FOUND:
      return "ITEM_NOT_FOUND";
    case ViolationType::NO_USER:
      return "NO_USER";
    case ViolationType::NO_USER_NO_RECEIPT:
      return "NO_USER_NO_RECEIPT";
    case ViolationType::NO_USER_SPECIAL_ITEM:
      return "NO_USER_SPECIAL_ITEM";
    case ViolationType::NO_RECEIPT:
      return "NO_RECEIPT";
    case ViolationType::ITEM_IS_SPECIAL:
      return "ITEM_IS_SPECIAL";
    case ViolationType::ITEM_SPECIAL_WRONG_SPECIFIC:
      return "ITEM_SPECIAL_WRONG_SPECIFIC";
    case ViolationType::CORRECT:
      return "CORRECT";
  }
}

}  // namespace cart

}  // namespace code_architecture::models