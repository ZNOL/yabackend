#pragma once

#include <userver/components/component_list.hpp>
#include <userver/components/loggable_component_base.hpp>

#include "models.hpp"
#include "service_validator.hpp"
#include "storage_accounts.hpp"
#include "storage_medicaments.hpp"

namespace code_architecture::service {

class PharmacyCartChecker final
    : public userver::components::LoggableComponentBase {
 public:
  static constexpr std::string_view kName = "pharmacy-cart-checker";

  PharmacyCartChecker(const userver::components::ComponentConfig& config,
                      const userver::components::ComponentContext& context);

  ~PharmacyCartChecker() final;

  void Check(models::cart::Cart& cart);

 private:
  storage::Medicaments& medicaments_storage_;
  storage::Accounts& accounts_storage_;
};

}  // namespace code_architecture::service