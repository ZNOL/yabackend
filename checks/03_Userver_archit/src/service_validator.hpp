#pragma once

#include <iostream>
#include <variant>

#include "models.hpp"

namespace code_architecture::service {

template <class... Ts>
struct Visitor : Ts... {
  using Ts::operator()...;
};

template <class... Ts>
Visitor(Ts...) -> Visitor<Ts...>;

constexpr auto Checker = Visitor{
    [](const models::NoAccount&, const models::items::Common&) {
      return models::cart::ViolationType::NO_USER;
    },

    [](const models::NoAccount&, const models::items::Recipe&) {
      return models::cart::ViolationType::NO_USER_NO_RECEIPT;
    },

    [](const models::NoAccount&, const models::items::Special&) {
      return models::cart::ViolationType::NO_USER_SPECIAL_ITEM;
    },

    [](const models::users::Customer&, const models::NoItem&) {
      return models::cart::ViolationType::ITEM_NOT_FOUND;
    },

    [](const models::users::Doctor&, const models::NoItem&) {
      return models::cart::ViolationType::ITEM_NOT_FOUND;
    },

    [](const models::users::Customer& customer,
       const models::items::Recipe& item) {
      return customer.recipes_.count(item.id_)
                 ? models::cart::ViolationType::CORRECT
                 : models::cart::ViolationType::NO_RECEIPT;
    },

    [](const models::users::Customer&, const models::items::Special&) {
      return models::cart::ViolationType::ITEM_IS_SPECIAL;
    },

    [](const models::users::Customer&, const models::items::Common&) {
      return models::cart::ViolationType::CORRECT;
    },

    [](const models::users::Doctor&, const models::items::Common&) {
      return models::cart::ViolationType::CORRECT;
    },

    [](const models::users::Doctor&, const models::items::Recipe&) {
      return models::cart::ViolationType::CORRECT;
    },

    [](const models::users::Doctor& doctor,
       const models::items::Special& item) {
      return doctor.speciality_ == item.speciality_
                 ? models::cart::ViolationType::CORRECT
                 : models::cart::ViolationType::ITEM_SPECIAL_WRONG_SPECIFIC;
    },

    // side effect of compilation
    [](const std::monostate&,
       const std::monostate&) -> models::cart::ViolationType {
      throw std::runtime_error{"wrong visit"};
    },
};

models::cart::ViolationType Validate(const models::Account& account,
                                     const models::Medicament& medicament);

}  // namespace code_architecture::service