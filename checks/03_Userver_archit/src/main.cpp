#include <userver/clients/http/component.hpp>
#include <userver/components/minimal_server_component_list.hpp>
#include <userver/server/handlers/ping.hpp>
#include <userver/server/handlers/tests_control.hpp>
#include <userver/testsuite/testsuite_support.hpp>
#include <userver/utils/daemon_run.hpp>

#include "handler_check.hpp"
#include "storage_accounts.hpp"
#include "storage_medicaments.hpp"

int main(int argc, char* argv[]) {
  auto component_list =
      userver::components::MinimalServerComponentList()
          .Append<userver::server::handlers::Ping>()
          .Append<userver::components::TestsuiteSupport>()
          .Append<userver::components::HttpClient>()
          .Append<userver::server::handlers::TestsControl>()
          .Append<code_architecture::storage::Medicaments>()
          .Append<code_architecture::handlers::check::Handler>()
          .Append<code_architecture::service::PharmacyCartChecker>();

  code_architecture::storage::AppendAccounts(component_list);

  return userver::utils::DaemonMain(argc, argv, component_list);
}
