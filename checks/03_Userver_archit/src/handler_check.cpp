#include "handler_check.hpp"

namespace code_architecture::handlers::check {

Handler::Handler(const userver::components::ComponentConfig& config,
                 const userver::components::ComponentContext& component_context)
    : HttpHandlerJsonBase(config, component_context),
      checker_(component_context.FindComponent<service::PharmacyCartChecker>(
          "pharmacy-cart-checker")) {}

userver::formats::json::Value Handler::HandleRequestJsonThrow(
    const userver::server::http::HttpRequest& request,
    const userver::formats::json::Value&,
    userver::server::request::RequestContext&) const {
  models::cart::Cart cart{};
  cart.SetUserID(request.GetArg("user_id"));
  cart.SetItems(request.GetArgVector("item_id"));
  checker_.Check(cart);
  return MakeResponse(cart);
}

userver::formats::json::Value MakeResponse(const models::cart::Cart& cart) {
  userver::formats::json::ValueBuilder builder =
      userver::formats::json::ValueBuilder(
          userver::formats::common::Type::kArray);

  for (const auto& violation : cart.GetViolations()) {
    userver::formats::json::ValueBuilder value;
    value["item_id"] = violation.item_ ? violation.item_->raw_id_ : "";
    value["problem"] = models::cart::ViolationTypeToString(violation.problem_);
    builder.PushBack(std::move(value));
  }

  return builder.ExtractValue();
}

std::string MakeItemId(const models::cart::Violation& violation) {
  std::ostringstream os;
  if (violation.item_) {
  }
  return os.str();
}

}  // namespace code_architecture::handlers::check
