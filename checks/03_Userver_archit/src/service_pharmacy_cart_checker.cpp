#include "service_pharmacy_cart_checker.hpp"

namespace code_architecture::service {

PharmacyCartChecker::PharmacyCartChecker(
    const userver::components::ComponentConfig& config,
    const userver::components::ComponentContext& context)
    : LoggableComponentBase(config, context),
      medicaments_storage_(
          context.FindComponent<storage::Medicaments>("storage-medicaments")),
      accounts_storage_(
          context.FindComponent<storage::Accounts>("storage-accounts")) {}

void PharmacyCartChecker::Check(models::cart::Cart& cart) {
  auto account = accounts_storage_.GetAccount(cart.GetUserID());

  for (const auto& item : cart.GetItems()) {
    if (item.category_ == models::cart::Category::UNKNOWN) {
      cart.AddViolation(nullptr, models::cart::ViolationType::WRONG_CATEGORY);
      continue;
    }

    if (!item.id_.has_value()) {
      cart.AddViolation(nullptr,
                        models::cart::ViolationType::INCORRECT_ITEM_ID);
      continue;
    }

    auto medicament =
        medicaments_storage_.GetMedicament(item.id_.value(), item.category_);

    auto problem = Validate(account, medicament);
    if (problem != models::cart::ViolationType::CORRECT) {
      cart.AddViolation(std::make_shared<models::cart::Item>(item), problem);
    }
  }
}

PharmacyCartChecker::~PharmacyCartChecker() = default;

}  // namespace code_architecture::service