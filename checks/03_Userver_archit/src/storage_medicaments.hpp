#pragma once

#include <userver/clients/dns/component.hpp>
#include <userver/components/component_list.hpp>
#include <userver/components/loggable_component_base.hpp>
#include <userver/storages/postgres/cluster.hpp>
#include <userver/storages/postgres/component.hpp>

#include "models.hpp"
#include "sql.hpp"

namespace code_architecture::storage {

class Medicaments final : public userver::components::LoggableComponentBase {
 public:
  static constexpr std::string_view kName = "storage-medicaments";

  Medicaments(const userver::components::ComponentConfig& config,
              const userver::components::ComponentContext& context);
  ~Medicaments() final;

  models::Medicament GetMedicament(int64_t item_id,
                                   models::cart::Category category) const;
  std::optional<models::items::Common> FindCommonByID(int64_t item_id) const;
  std::optional<models::items::Recipe> FindRecipeByID(int64_t item_id) const;
  std::optional<models::items::Special> FindSpecialByID(int64_t item_id) const;

 private:
  userver::storages::postgres::ClusterPtr pg_cluster_;
};

}  // namespace code_architecture::storage