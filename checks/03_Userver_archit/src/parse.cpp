#include "parse.hpp"

namespace code_architecture {
std::string_view Strip(std::string_view s) {
  while (!s.empty() && isspace(s.front())) {
    s.remove_prefix(1);
  }
  while (!s.empty() && isspace(s.back())) {
    s.remove_suffix(1);
  }
  return s;
}

std::vector<std::string_view> SplitBy(std::string_view s,
                                      std::string_view delimiter) {
  std::vector<std::string_view> result;
  while (!s.empty()) {
    size_t pos = s.find(delimiter);
    result.push_back(Strip(s.substr(0, pos)));
    s.remove_prefix(pos != std::string::npos ? pos + delimiter.length()
                                             : s.size());
  }
  return result;
}
}  // namespace code_architecture