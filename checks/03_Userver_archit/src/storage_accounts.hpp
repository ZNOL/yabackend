#pragma once

#include <userver/clients/dns/component.hpp>
#include <userver/components/component_list.hpp>
#include <userver/components/loggable_component_base.hpp>
#include <userver/storages/postgres/cluster.hpp>
#include <userver/storages/postgres/component.hpp>

#include "models.hpp"
#include "sql.hpp"

namespace code_architecture::storage {

class Accounts final : public userver::components::LoggableComponentBase {
 public:
  static constexpr std::string_view kName = "storage-accounts";

  Accounts(const userver::components::ComponentConfig& config,
           const userver::components::ComponentContext& context);
  ~Accounts() final;

  models::Account GetAccount(int64_t user_id) const;
  std::optional<models::users::Customer> FindCustomerByID(
      int64_t user_id) const;
  std::optional<models::users::Doctor> FindDoctorByID(int64_t user_id) const;

 private:
  userver::storages::postgres::ClusterPtr pg_cluster_;
};

void AppendAccounts(userver::components::ComponentList& component_list);

}  // namespace code_architecture::storage
