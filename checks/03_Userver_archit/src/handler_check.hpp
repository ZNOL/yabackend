#pragma once

#include <string>
#include <string_view>

#include <userver/formats/serialize/common_containers.hpp>
#include <userver/server/handlers/http_handler_json_base.hpp>

#include "models.hpp"
#include "request.hpp"
#include "service_pharmacy_cart_checker.hpp"

namespace code_architecture::handlers::check {

class Handler final : public userver::server::handlers::HttpHandlerJsonBase {
 public:
  static constexpr std::string_view kName = "handler-check";

  Handler(const userver::components::ComponentConfig& config,
          const userver::components::ComponentContext& component_context);

  userver::formats::json::Value HandleRequestJsonThrow(
      const userver::server::http::HttpRequest& request,
      const userver::formats::json::Value&,
      userver::server::request::RequestContext&) const;

 private:
  service::PharmacyCartChecker& checker_;
};

userver::formats::json::Value MakeResponse(const models::cart::Cart& cart);

}  // namespace code_architecture::handlers::check
