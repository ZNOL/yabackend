#include <bits/stdc++.h>

using namespace std;
namespace fast {
#define iom    \
  cin.tie(0);  \
  cout.tie(0); \
  ios_base::sync_with_stdio(0);
#define out cout.flush();
#define endl "\n"

using ll = long long int;
using ld = long double;

// #define MULTITESTS
// #define FILENAME "graph"
// #define int long long   // for emergencies only

template <typename T>
using pp = std::pair<T, T>;

#define sqr(x) (x) * (x)
#define sz(x) (int)(x).size()
#define all(x) x.begin(), x.end()
}  // namespace fast
using namespace fast;

int maxSegmnent(int a[110][110], int x1, int y1, int x2, int y2) {
  int ans = -100;

  for (int i = x1; i < x2; ++i) {
    for (int j = y1; j < y2; ++j) {
      if (ans < a[i][j]) {
        ans = a[i][j];
      }
    }
  }

  return ans;
}

void solve() {
  int n, m;
  int a[110][110];

  cin >> n >> m;
  for (int i = 0; i < n; ++i) {
    for (int j = 0; j < m; ++j) {
      cin >> a[i][j];
    }
  }
  int q;
  cin >> q;
  while (q--) {
    int x1, y1, x2, y2;
    cin >> x1 >> y1 >> x2 >> y2;
    --x1, --y1, --x2, --y2;
    cout << maxSegmnent(a, x1, y1, x2, y2) << endl;
  }
}

int32_t main() {
  iom;

#ifdef FILENAME
  freopen(FILENAME ".in", "r", stdin);
  freopen(FILENAME ".out", "w", stdout);
#endif  // FILENAME

  int T = 1;
#ifdef MULTITESTS
  cin >> T;
#endif  // MULTITESTS
  while (T--) {
    solve();
  }
  out;
  return 0;
}
