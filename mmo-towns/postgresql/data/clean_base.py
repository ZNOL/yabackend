def check228(word):
    for c in s:
        if word in c:
            return True
    return False


def check1(curWord):
    for seq in all_cities:
        if curWord in seq:
            ans = ''
            for word in seq:
                if check228(word):
                    ans = word 
                    break
            return ans


s = set()
all_cities = []

print(f'LEN s = {len(s)}')
print(f'LEN cities = {len(all_cities)}')

clean_s = set()
i = 0
for c in s:
    name1, name2 = c 
    print(i)
    i += 1
    if ord('а') <= ord(name1[0]) <= ord('я'):
        c = check1(name1)
        if c:
            clean_s.add((name1.lower(), c.lower()))


start = "--\n-- PostgreSQL database dump\n--\n\n-- Dumped from database version 9.3rc1\n-- Dumped by pg_dump version 9.3rc1\n-- Started on 2013-12-07 16:13:13\n\nSET statement_timeout = 0;\nSET lock_timeout = 0;\nSET client_encoding = 'UTF8';\nSET standard_conforming_strings = on;\nSET check_function_bodies = false;\nSET client_min_messages = warning;\n\nSET search_path = public, pg_catalog;\n\n--\n-- TOC entry 1940 (class 0 OID 51147)\n-- Dependencies: 172\n-- Data for Name: _cities; Type: TABLE DATA; Schema: public; Owner: postgres\n--\n"
print(start, file=out_file)
cur_id = 0
for c in clean_s:
    cur_id += 1
    print(f"INSERT INTO _cities (city_id, title_ru, title_en) VALUES ({cur_id}, '{c[0]}', '{c[1]}');", file=out_file)
out_file.close()

