from json import loads 


def find228(word):
    for seq in ccities:
        if word in seq:
            for prob_word in seq:
                if prob_word in english_ss:
                    return prob_word
    return ''


b1 = open('base1.txt')
b2 = open('base2.txt')

s = loads(b1.readline())
cities = loads(b2.readline())

# ccities = [set(i for i in seq if len(i) > 3 and all(ord('a') <= ord(j) <= ord('z') for j in i.lower())) for seq in cities]
ccities = [set(i for i in seq if len(i) > 3) for seq in cities]

ss = set()
english_ss = set()
for names in s:
    name1, name2 = names
    if ord("а") <= ord(name1[0]) <= ord("я"):
        ss.add(name1)
    else:
        english_ss.add(name2)

print(len(ss), len(english_ss))
clean_s = set()
i = 0
all_ru = set()
for town in ss: 
    ans = find228(town)
    if ans:
        clean_s.add((town, ans))
    else:
        clean_s.add((town, 'NULL'))
    print(i, len(clean_s))
    i += 1



out_file = open('cities.sql', 'w')
start = "--\n-- PostgreSQL database dump\n--\n\n-- Dumped from database version 9.3rc1\n-- Dumped by pg_dump version 9.3rc1\n-- Started on 2013-12-07 16:13:13\n\nSET statement_timeout = 0;\nSET lock_timeout = 0;\nSET client_encoding = 'UTF8';\nSET standard_conforming_strings = on;\nSET check_function_bodies = false;\nSET client_min_messages = warning;\n\nSET search_path = public, pg_catalog;\n\n--\n-- TOC entry 1940 (class 0 OID 51147)\n-- Dependencies: 172\n-- Data for Name: _cities; Type: TABLE DATA; Schema: public; Owner: postgres\n--\n"
print(start, file=out_file)
cur_id = 0
for c in clean_s:
    c1, c2 = c[0].lower(), c[1].lower()
    cur_id += 1
    if c2 != 'null':
        print(f"INSERT INTO towns.city (id, name_ru_RU, name_en_EN) VALUES ({cur_id}, '{c1}', '{c2}');", file=out_file)
    else:
        print(f"INSERT INTO towns.city (id, name_ru_RU, name_en_EN) VALUES ({cur_id}, '{c1}', NULL);", file=out_file)
out_file.close()


