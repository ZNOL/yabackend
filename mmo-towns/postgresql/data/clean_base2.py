from json import loads, dumps

s = set()
with open('_cities.sql') as file:
    for line in file.readlines():
        if line.startswith('INSERT'):
            y = line.split('VALUES')[1].split(',')
            city_id = y[0].strip().replace('(', '').replace(',', '').replace("'", "")
            title_ru = y[4].strip().replace('(', '').replace(',', '').replace("'", "")
            title_en = y[13].strip().replace('(', '').replace(',', '').replace("'", "")
            s.add((title_ru.lower(), title_en.lower()))

all_cities = []
with open('cities5000.txt') as file:
    for line in file.readlines():
        seq = set()
        for tmp in line.split():
            for word in tmp.split(','):
                if word.isalpha():
                    seq.add(word.lower())
        all_cities.append(list(i for i in seq))


file1 = open('base1.txt', 'w')
print(dumps(list(i for i in s)), file=file1)

file2 = open('base2.txt', 'w')
print(dumps(all_cities), file=file2)

