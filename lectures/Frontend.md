* Git
  * ((https://disk.yandex.ru/i/r4SHPbBcZGyUeA запись лекции))
* Асинхронность
  * ((https://disk.yandex.ru/i/CdJKqjHUxveF-A запись лекции))
  * ((file:/frontend/zapis-lekcijj-i-prezentacii/asinxronnostvjs.pdf презентация))
  * ((https://disk.yandex.ru/i/ka0zWQ3n8y2OUQ разбор ДЗ))
* Node.js
  * ((https://disk.yandex.ru/d/92D29UJb2FC7bA запись лекции))
  * ((file:/frontend/zapis-lekcijj-i-prezentacii/volkov-nodejs.pdf презентация)) 
* React (базовый)
  * ((https://disk.yandex.ru/i/c_4A1RnlRgut-Q запись лекции))
  * ((https://wiki.yandex.ru/users/fall-schools-2022/react/.files/reactbasefinal1.pdf презентация))
* React (продвинутый)
  * ((https://disk.yandex.ru/i/pob43MPXdwiJKQ запись лекции))
  * ((https://wiki.yandex.ru/users/fall-schools-2022/react/.files/reactprofinal.pdf презентация))
* React (построение React-приложения)
  * ((https://disk.yandex.ru/d/ol9MQ4OUHWn3Fw запись лекции))
  * ((https://disk.yandex.ru/d/36kSa2Gv_brhbw разбор ДЗ))
* Тесты
  * ((https://disk.yandex.ru/i/cvdZpYDoPXJiSA запись лекции))
  * модульные: ((file:/frontend/zapis-lekcijj-i-prezentacii/moduletests.pdf презентация))
  * интеграционные: ((file:/frontend/zapis-lekcijj-i-prezentacii/integrationtests.pdf презентация))
  * ((https://disk.yandex.ru/d/sfc2KC7U6gTm3A разбор ДЗ Тесты + Типизация))
* Типизация
  * ((https://disk.yandex.ru/i/kxcyLjAlhoSkZw запись лекции))
  * ((file:/frontend/zapis-lekcijj-i-prezentacii/lection11.pptx презентация))
  * ((https://disk.yandex.ru/d/sfc2KC7U6gTm3A разбор ДЗ Тесты + Типизация))
* Инфраструктура
  * ((https://disk.yandex.ru/i/NsyK9Qk3jL9Hxw запись лекции))
  * ((file:/frontend/zapis-lekcijj-i-prezentacii/shriinfrastruktura.pdf презентация))
* Продвинутый JS
  * ((https://disk.yandex.ru/i/L9LARd-ZQ84kKw запись лекции))
  * ((file:/frontend/zapis-lekcijj-i-prezentacii/prodvinutyijavascript-1.pdf презентация))
* Функциональное программирование
  * ((https://disk.yandex.ru/i/7uk-ipUr1f3KvQ запись лекции))
  * ((file:/frontend/zapis-lekcijj-i-prezentacii/karriruikompoziruiochishhai.pdf презентация))
* Сборка
  * ((https://disk.yandex.ru/i/yJI2oe7HxlKOFw запись лекции))
  * ((file:/frontend/zapis-lekcijj-i-prezentacii/shribuild2022.pdf презентация))
* Performance
  * ((https://disk.yandex.ru/i/qqFVaDOhmOFk-w запись лекции))
  * ((file:/frontend/zapis-lekcijj-i-prezentacii/performance2022.2.pdf презентация))


