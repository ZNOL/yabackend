#!/bin/bash

cd build
dpkg-buildpackage -b --no-sign >/dev/null 2>&1

cd /var/lib/dpkg/info

sudo dpkg -i mountain-quebec-arizona-moon_1.0.0-1_amd64.deb 

/usr/bin/shbr-echo -c /etc/shbr-echo/cpp/configs/static_config.yaml --config_vars /etc/shbr-echo/cpp/configs/config_vars.yaml
