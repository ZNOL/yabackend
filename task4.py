import requests
import logging
from random import random
import time

SERVER_URL = '123.123.123.123'
SERVER_PORT = 1234
MAX_TRIES = 10
SLEEP_TIME = 0.5

class EXCEPTION_500(Exception):
    pass

class EXCEPTION_404(Exception):
    pass

def handler_get(url : str, headers : dict):
    result = request.get(f'{SERVER_URL}/{url}', headers=headers)
    if result.code == 200:
        result_json = result.json()
        return result_json
    elif result.code == 501:
        throw EXCEPTION_500('Wrong request')
    elif result.code == 404:
        throw EXCEPTION_404('Server_error')
    else:
        throw EXCEPTION('Unknown code')


def handler_post(url : str, headers : dict):
    pass


def handler(url : str, method : str, headers : dict):
    func = {
        'GET': handler_get,
        'POST': handler_post,
    }

    if method in func:
        cur_tries = 0
        cur_sleep = SLEEP_TIME
        while cur_tries < MAX_TRIES:
            cur_tries += 1
            try:
                return func[method](url, headers)
            except EXCEPTION_404 as e:
                return {'error': 'Server offline'}
            except Exception as e:
                logging.error(f'METHOD : {method}, URL : {url}, MESSAGE : {e.str()}')
            time.sleep(cur_sleep + cur_sleep * random())
            cur_sleep *= 1.5
        return {'error': 'Max tries exceded'}
    else:
        return {'error': 'Wrong method'}
        

                
