gmake[1]: Entering directory '/bookmarker/build_debug'
/usr/bin/cmake -S/bookmarker -B/bookmarker/build_debug --check-build-system CMakeFiles/Makefile.cmake 0
Re-run cmake file: Makefile older than: ../third_party/userver/core/CMakeLists.txt
-- C compiler: /usr/bin/cc
-- C++ compiler: /usr/bin/c++
-- LTO: disabled (local build)
-- ccache: enabled
-- variant: 1
-- Found Boost: /usr/lib/x86_64-linux-gnu/cmake/Boost-1.74.0/BoostConfig.cmake (found version "1.74.0")  
-- Checking -ftemplate-backtrace-limit=0
-- Checking -ftemplate-backtrace-limit=0 - found
-- Checking -Wdisabled-optimization
-- Checking -Wdisabled-optimization - found
-- Checking -Winvalid-pch
-- Checking -Winvalid-pch - found
-- Checking -Wlogical-op
-- Checking -Wlogical-op - found
-- Checking -Wformat=2
-- Checking -Wformat=2 - found
-- Checking -Wno-error=deprecated-declarations
-- Checking -Wno-error=deprecated-declarations - found
-- Checking -Wimplicit-fallthrough
-- Checking -Wimplicit-fallthrough - found
-- Checking -Wno-useless-cast
-- Checking -Wno-useless-cast - found
-- boost: 1.74.0
-- Building utest with gtest and ubench with gbench
-- Building userver as a subproject
-- Sanitizers are ON: addr ub
-- Python: /usr/bin/python3.9
-- Generating cmake files ...
-- Userver version 1.0.0
-- DWCAS: Using Boost.Atomic
-- DWCAS works
-- Found Boost: /usr/lib/x86_64-linux-gnu/cmake/Boost-1.74.0/BoostConfig.cmake (found version "1.74.0") found components: program_options filesystem locale regex iostreams 
-- Could not find `CryptoPP` package.
	Debian: sudo apt update && sudo apt install libcrypto++-dev
	MacOS: brew install cryptopp
	Fedora: sudo dnf install cryptopp-devel
	ArchLinux: sudo pacman -S crypto++
 (missing: CryptoPP_LIBRARIES CryptoPP_INCLUDE_DIRS) 
-- Downloading CryptoPP from remote
-- CMake version 3.18.4
-- System Linux
-- Processor x86_64
-- Performing Test CRYPTOPP_X86_SSE2
-- Performing Test CRYPTOPP_X86_SSE2 - Success
-- Performing Test CRYPTOPP_X86_SSSE3
-- Performing Test CRYPTOPP_X86_SSSE3 - Success
-- Performing Test CRYPTOPP_X86_SSE41
-- Performing Test CRYPTOPP_X86_SSE41 - Success
-- Performing Test CRYPTOPP_X86_SSE42
-- Performing Test CRYPTOPP_X86_SSE42 - Success
-- Performing Test CRYPTOPP_X86_CLMUL
-- Performing Test CRYPTOPP_X86_CLMUL - Success
-- Performing Test CRYPTOPP_X86_AES
-- Performing Test CRYPTOPP_X86_AES - Success
-- Performing Test CRYPTOPP_X86_AVX
-- Performing Test CRYPTOPP_X86_AVX - Success
-- Performing Test CRYPTOPP_X86_AVX2
-- Performing Test CRYPTOPP_X86_AVX2 - Success
-- Performing Test CRYPTOPP_X86_SHA
-- Performing Test CRYPTOPP_X86_SHA - Success
-- Performing Test CRYPTOPP_MIXED_ASM
-- Performing Test CRYPTOPP_MIXED_ASM - Success
-- Platform: x86_64
-- Compiler: /usr/bin/c++
-- Compiler options:  
-- Compiler definitions: 
-- Build type: Debug
-- Putting userver into namespace 'userver': namespace userver { }
-- Found Boost: /usr/lib/x86_64-linux-gnu/cmake/Boost-1.74.0/BoostConfig.cmake (found version "1.74.0")  
-- Context impl: ucontext
-- Found Boost: /usr/lib/x86_64-linux-gnu/cmake/Boost-1.74.0/BoostConfig.cmake (found version "1.74.0") found components: regex 
-- dpkg-query: no packages found matching protoc-dev

-- Could NOT find PkgConfig (missing: PKG_CONFIG_EXECUTABLE) 
-- Failed to find 'pkg-config'
-- Downloading clickhouse-cpp from remote
-- Downloading amqp-cpp from remote
-- Found Boost: /usr/lib/x86_64-linux-gnu/cmake/Boost-1.74.0/BoostConfig.cmake (found version "1.74.0") found components: program_options filesystem regex 
-- Putting userver into namespace 'userver': namespace userver { }
-- Python: /usr/bin/python3.9
-- Setting up the virtualenv with requirements:
--   /bookmarker/tests/requirements.txt
Requirement already satisfied: yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from -r /bookmarker/tests/requirements.txt (line 1)) (0.1.6.4)
Collecting yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3
  Downloading yandex_taxi_testsuite-0.1.6.4-py3-none-any.whl (113 kB)
  Downloading yandex_taxi_testsuite-0.1.6.3-py3-none-any.whl (113 kB)
Requirement already satisfied: pytest>=4.5.0 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (7.2.0)
Requirement already satisfied: aiohttp>=3.5.4 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (3.8.3)
Requirement already satisfied: pytest-aiohttp>=0.3.0 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (1.0.4)
Requirement already satisfied: pytz>=2018.5 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (2022.5)
Requirement already satisfied: PyYAML>=3.13 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (6.0)
Requirement already satisfied: python-dateutil>=2.7.3 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (2.8.2)
Requirement already satisfied: cached-property>=1.5.1 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (1.5.2)
Requirement already satisfied: pymongo>=3.7.1 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (4.3.2)
Requirement already satisfied: uvloop>=0.12.1 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (0.17.0)
Requirement already satisfied: yarl!=1.6,>=1.4.2 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (1.8.1)
Requirement already satisfied: psycopg2-binary>=2.7.5 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (2.9.5)
Requirement already satisfied: async-timeout<5.0,>=4.0.0a3 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from aiohttp>=3.5.4->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (4.0.2)
Requirement already satisfied: attrs>=17.3.0 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from aiohttp>=3.5.4->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (22.1.0)
Requirement already satisfied: charset-normalizer<3.0,>=2.0 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from aiohttp>=3.5.4->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (2.1.1)
Requirement already satisfied: multidict<7.0,>=4.5 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from aiohttp>=3.5.4->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (6.0.2)
Requirement already satisfied: frozenlist>=1.1.1 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from aiohttp>=3.5.4->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (1.3.1)
Requirement already satisfied: aiosignal>=1.1.2 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from aiohttp>=3.5.4->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (1.2.0)
Requirement already satisfied: dnspython<3.0.0,>=1.16.0 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from pymongo>=3.7.1->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (2.2.1)
Requirement already satisfied: exceptiongroup>=1.0.0rc8 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from pytest>=4.5.0->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (1.0.0rc9)
Requirement already satisfied: pluggy<2.0,>=0.12 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from pytest>=4.5.0->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (1.0.0)
Requirement already satisfied: packaging in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from pytest>=4.5.0->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (21.3)
Requirement already satisfied: iniconfig in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from pytest>=4.5.0->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (1.1.1)
Requirement already satisfied: tomli>=1.0.0 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from pytest>=4.5.0->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (2.0.1)
Requirement already satisfied: pytest-asyncio>=0.17.2 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from pytest-aiohttp>=0.3.0->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (0.20.1)
Requirement already satisfied: six>=1.5 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from python-dateutil>=2.7.3->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (1.16.0)
Requirement already satisfied: idna>=2.0 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from yarl!=1.6,>=1.4.2->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (3.4)
Requirement already satisfied: pyparsing!=3.0.5,>=2.0.2 in ./tests/venv-testsuite-bookmarker/lib/python3.9/site-packages (from packaging->pytest>=4.5.0->yandex-taxi-testsuite[postgresql-binary]>=0.1.6.3->-r /bookmarker/tests/requirements.txt (line 1)) (3.0.9)
-- Set install prefix: ~/.local
-- Configuring done
-- Generating done
-- Build files have been written to: /bookmarker/build_debug
/usr/bin/gmake  -f CMakeFiles/Makefile2 bookmarker
gmake[2]: Entering directory '/bookmarker/build_debug'
/usr/bin/cmake -S/bookmarker -B/bookmarker/build_debug --check-build-system CMakeFiles/Makefile.cmake 0
/usr/bin/cmake -E cmake_progress_start /bookmarker/build_debug/CMakeFiles 66
/usr/bin/gmake  -f CMakeFiles/Makefile2 CMakeFiles/bookmarker.dir/all
gmake[3]: Entering directory '/bookmarker/build_debug'
/usr/bin/gmake  -f userver/postgresql/pq-extra/CMakeFiles/userver-pq-extra.dir/build.make userver/postgresql/pq-extra/CMakeFiles/userver-pq-extra.dir/depend
/usr/bin/gmake  -f third_party/userver/uboost_coro/CMakeFiles/userver-uboost-coro.dir/build.make third_party/userver/uboost_coro/CMakeFiles/userver-uboost-coro.dir/depend
/usr/bin/gmake  -f third_party/cryptopp/CMakeFiles/cryptopp-static.dir/build.make third_party/cryptopp/CMakeFiles/cryptopp-static.dir/depend
/usr/bin/gmake  -f third_party/userver/third_party/boost_stacktrace/CMakeFiles/userver-stacktrace.dir/build.make third_party/userver/third_party/boost_stacktrace/CMakeFiles/userver-stacktrace.dir/depend
/usr/bin/gmake  -f third_party/userver/third_party/compiler-rt/CMakeFiles/userver-compiler-rt-parts.dir/build.make third_party/userver/third_party/compiler-rt/CMakeFiles/userver-compiler-rt-parts.dir/depend
gmake[4]: Entering directory '/bookmarker/build_debug'
cd /bookmarker/build_debug && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /bookmarker /bookmarker/third_party/userver/postgresql/pq-extra /bookmarker/build_debug /bookmarker/build_debug/userver/postgresql/pq-extra /bookmarker/build_debug/userver/postgresql/pq-extra/CMakeFiles/userver-pq-extra.dir/DependInfo.cmake --color=
gmake[4]: Entering directory '/bookmarker/build_debug'
cd /bookmarker/build_debug && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /bookmarker /bookmarker/third_party/userver/uboost_coro /bookmarker/build_debug /bookmarker/build_debug/third_party/userver/uboost_coro /bookmarker/build_debug/third_party/userver/uboost_coro/CMakeFiles/userver-uboost-coro.dir/DependInfo.cmake --color=
gmake[4]: Entering directory '/bookmarker/build_debug'
cd /bookmarker/build_debug && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /bookmarker /bookmarker/third_party/userver/third_party/boost_stacktrace /bookmarker/build_debug /bookmarker/build_debug/third_party/userver/third_party/boost_stacktrace /bookmarker/build_debug/third_party/userver/third_party/boost_stacktrace/CMakeFiles/userver-stacktrace.dir/DependInfo.cmake --color=
gmake[4]: Entering directory '/bookmarker/build_debug'
cd /bookmarker/build_debug && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /bookmarker /bookmarker/third_party/userver/third_party/compiler-rt /bookmarker/build_debug /bookmarker/build_debug/third_party/userver/third_party/compiler-rt /bookmarker/build_debug/third_party/userver/third_party/compiler-rt/CMakeFiles/userver-compiler-rt-parts.dir/DependInfo.cmake --color=
gmake[4]: Leaving directory '/bookmarker/build_debug'
gmake[4]: Leaving directory '/bookmarker/build_debug'
/usr/bin/gmake  -f third_party/userver/third_party/boost_stacktrace/CMakeFiles/userver-stacktrace.dir/build.make third_party/userver/third_party/boost_stacktrace/CMakeFiles/userver-stacktrace.dir/build
gmake[4]: Leaving directory '/bookmarker/build_debug'
/usr/bin/gmake  -f third_party/userver/uboost_coro/CMakeFiles/userver-uboost-coro.dir/build.make third_party/userver/uboost_coro/CMakeFiles/userver-uboost-coro.dir/build
/usr/bin/gmake  -f third_party/userver/third_party/compiler-rt/CMakeFiles/userver-compiler-rt-parts.dir/build.make third_party/userver/third_party/compiler-rt/CMakeFiles/userver-compiler-rt-parts.dir/build
gmake[4]: Leaving directory '/bookmarker/build_debug'
/usr/bin/gmake  -f userver/postgresql/pq-extra/CMakeFiles/userver-pq-extra.dir/build.make userver/postgresql/pq-extra/CMakeFiles/userver-pq-extra.dir/build
gmake[4]: Entering directory '/bookmarker/build_debug'
gmake[4]: Nothing to be done for 'third_party/userver/third_party/boost_stacktrace/CMakeFiles/userver-stacktrace.dir/build'.
gmake[4]: Leaving directory '/bookmarker/build_debug'
gmake[4]: Entering directory '/bookmarker/build_debug'
gmake[4]: Nothing to be done for 'third_party/userver/third_party/compiler-rt/CMakeFiles/userver-compiler-rt-parts.dir/build'.
gmake[4]: Leaving directory '/bookmarker/build_debug'
gmake[4]: Entering directory '/bookmarker/build_debug'
gmake[4]: Nothing to be done for 'userver/postgresql/pq-extra/CMakeFiles/userver-pq-extra.dir/build'.
gmake[4]: Leaving directory '/bookmarker/build_debug'
gmake[4]: Entering directory '/bookmarker/build_debug'
gmake[4]: Nothing to be done for 'third_party/userver/uboost_coro/CMakeFiles/userver-uboost-coro.dir/build'.
gmake[4]: Leaving directory '/bookmarker/build_debug'
[  3%] Built target userver-uboost-coro
[  3%] Built target userver-pq-extra
[  3%] Built target userver-stacktrace
[  3%] Built target userver-compiler-rt-parts
gmake[4]: Entering directory '/bookmarker/build_debug'
cd /bookmarker/build_debug && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /bookmarker /bookmarker/third_party/userver/third_party/cryptopp /bookmarker/build_debug /bookmarker/build_debug/third_party/cryptopp /bookmarker/build_debug/third_party/cryptopp/CMakeFiles/cryptopp-static.dir/DependInfo.cmake --color=
gmake[4]: Leaving directory '/bookmarker/build_debug'
/usr/bin/gmake  -f third_party/cryptopp/CMakeFiles/cryptopp-static.dir/build.make third_party/cryptopp/CMakeFiles/cryptopp-static.dir/build
gmake[4]: Entering directory '/bookmarker/build_debug'
gmake[4]: Nothing to be done for 'third_party/cryptopp/CMakeFiles/cryptopp-static.dir/build'.
gmake[4]: Leaving directory '/bookmarker/build_debug'
[ 30%] Built target cryptopp-static
/usr/bin/gmake  -f userver/core/CMakeFiles/userver-core.dir/build.make userver/core/CMakeFiles/userver-core.dir/depend
gmake[4]: Entering directory '/bookmarker/build_debug'
cd /bookmarker/build_debug && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /bookmarker /bookmarker/third_party/userver/core /bookmarker/build_debug /bookmarker/build_debug/userver/core /bookmarker/build_debug/userver/core/CMakeFiles/userver-core.dir/DependInfo.cmake --color=
gmake[4]: Leaving directory '/bookmarker/build_debug'
/usr/bin/gmake  -f userver/core/CMakeFiles/userver-core.dir/build.make userver/core/CMakeFiles/userver-core.dir/build
gmake[4]: Entering directory '/bookmarker/build_debug'
gmake[4]: Nothing to be done for 'userver/core/CMakeFiles/userver-core.dir/build'.
gmake[4]: Leaving directory '/bookmarker/build_debug'
[ 90%] Built target userver-core
/usr/bin/gmake  -f userver/postgresql/CMakeFiles/userver-postgresql.dir/build.make userver/postgresql/CMakeFiles/userver-postgresql.dir/depend
gmake[4]: Entering directory '/bookmarker/build_debug'
cd /bookmarker/build_debug && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /bookmarker /bookmarker/third_party/userver/postgresql /bookmarker/build_debug /bookmarker/build_debug/userver/postgresql /bookmarker/build_debug/userver/postgresql/CMakeFiles/userver-postgresql.dir/DependInfo.cmake --color=
gmake[4]: Leaving directory '/bookmarker/build_debug'
/usr/bin/gmake  -f userver/postgresql/CMakeFiles/userver-postgresql.dir/build.make userver/postgresql/CMakeFiles/userver-postgresql.dir/build
gmake[4]: Entering directory '/bookmarker/build_debug'
gmake[4]: Nothing to be done for 'userver/postgresql/CMakeFiles/userver-postgresql.dir/build'.
gmake[4]: Leaving directory '/bookmarker/build_debug'
[ 98%] Built target userver-postgresql
/usr/bin/gmake  -f CMakeFiles/bookmarker_objs.dir/build.make CMakeFiles/bookmarker_objs.dir/depend
gmake[4]: Entering directory '/bookmarker/build_debug'
cd /bookmarker/build_debug && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /bookmarker /bookmarker /bookmarker/build_debug /bookmarker/build_debug /bookmarker/build_debug/CMakeFiles/bookmarker_objs.dir/DependInfo.cmake --color=
gmake[4]: Leaving directory '/bookmarker/build_debug'
/usr/bin/gmake  -f CMakeFiles/bookmarker_objs.dir/build.make CMakeFiles/bookmarker_objs.dir/build
gmake[4]: Entering directory '/bookmarker/build_debug'
gmake[4]: Nothing to be done for 'CMakeFiles/bookmarker_objs.dir/build'.
gmake[4]: Leaving directory '/bookmarker/build_debug'
[100%] Built target bookmarker_objs
/usr/bin/gmake  -f CMakeFiles/bookmarker.dir/build.make CMakeFiles/bookmarker.dir/depend
gmake[4]: Entering directory '/bookmarker/build_debug'
cd /bookmarker/build_debug && /usr/bin/cmake -E cmake_depends "Unix Makefiles" /bookmarker /bookmarker /bookmarker/build_debug /bookmarker/build_debug /bookmarker/build_debug/CMakeFiles/bookmarker.dir/DependInfo.cmake --color=
gmake[4]: Leaving directory '/bookmarker/build_debug'
/usr/bin/gmake  -f CMakeFiles/bookmarker.dir/build.make CMakeFiles/bookmarker.dir/build
gmake[4]: Entering directory '/bookmarker/build_debug'
gmake[4]: Nothing to be done for 'CMakeFiles/bookmarker.dir/build'.
gmake[4]: Leaving directory '/bookmarker/build_debug'
[100%] Built target bookmarker
gmake[3]: Leaving directory '/bookmarker/build_debug'
/usr/bin/cmake -E cmake_progress_start /bookmarker/build_debug/CMakeFiles 0
gmake[2]: Leaving directory '/bookmarker/build_debug'
gmake[1]: Leaving directory '/bookmarker/build_debug'
-- Install configuration: "Debug"
-- Installing: /home/user/.local/bin/bookmarker
-- Installing: /home/user/.local/etc/bookmarker/config_vars.docker.yaml
-- Installing: /home/user/.local/etc/bookmarker/config_vars.yaml
-- Installing: /home/user/.local/etc/bookmarker/config_vars_testing.yaml
-- Installing: /home/user/.local/etc/bookmarker/dynamic_config_fallback.json
-- Installing: /home/user/.local/etc/bookmarker/static_config.yaml
CREATE EXTENSION
DROP SCHEMA
CREATE SCHEMA
CREATE TABLE
CREATE TABLE
CREATE TABLE
