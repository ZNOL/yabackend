import aiohttp

import pytest

# Start the tests via `make test-debug` or `make test-release`


async def test_multipart_form_data(service_client):
    with aiohttp.MultipartWriter('form-data') as data:
        payload = aiohttp.payload.StringPayload('test')
        payload.set_content_disposition('form-data', name='email')
        data.append_payload(payload)

    headers = {
        'Content-Type': 'multipart/form-data; boundary=' + data.boundary,
    }

    response = await service_client.post(
        '/register', 
        data=data,
        headers=headers
    )

    assert response.status == 200


@pytest.mark.parametrize(
 'email, password', [
    {"me@ya.ru", "passwd"},
    {"123@ya.ru", "1"},
    {"2", "passwd"}
 ]
)
async def test_register(service_client, email, password):
    data = {"email": email, "password": password}
    response = await service_client.post('/register', json=data)
    assert response.status == 200

    response_json = response.json()
    assert response_json["id"] != ''


async def test_double_register(service_client):
    data = {"email": "email@gmail.ru", "password": "72365823"}
    response = await service_client.post('/register', json=data)
    assert response.status == 200

    response = await service_client.post('/register', json=data)
    assert response.status != 500


async def test_login(service_client):
    data = {"email": "me@ya.ru", "password": "pass"}

    response = await service_client.post('/login', json=data)
    assert response.status == 404

    response = await service_client.post('/register', json=data)
    assert response.status == 200
    response = await service_client.post('/login', json=data)
    assert response.status == 200
    assert response.json()['id'] != ''


@pytest.mark.parametrize(
    'url, title, tag', [
        {"https://ya.ru", "Yandex", "Ya"},
        {"https://neya.ru", "NeYandex", ""},
        {"https://yaya.ru", "YaYandex", ""},
    ]
)
async def test_add_bookmark(service_client, url, title, tag):
    user_data = {"email": "me@ya.ru", "password": "pswd"}
    response = await service_client.post('/register', json=user_data)
    assert response.status == 200
    response = await service_client.post('/login', json=user_data)
    assert response.status == 200
    used_id = response.json()['id']
    headers = {
        "X-Ya-User-Ticket": used_id,
        "Content-Type": "application/json",
    }
    data = {
        "url": url,
        "title": title
    }
    if tag:
        data["tag"] = tag

    response = await service_client.post('/v1/bookmarks', json=data)
    assert response.status == 401

    response = await service_client.post('/v1/bookmarks', json=data, headers=headers)
    assert response.status == 200

    response_json = response.json()
    assert response_json['id'] != ''
    assert response_json['url'] == url
    assert response_json['title'] == title


@pytest.mark.parametrize(
    'limit, tag, length', [
        [10, '', 10],
        [7, '', 7],
        [3, 'a', 2],
    ]
)
async def test_get_bookmarks(service_client, limit, tag, length):
    user_data = {"email": "me@ya.ru", "password": "pswd"}
    response = await service_client.post('/register', json=user_data)
    assert response.status == 200
    response = await service_client.post('/login', json=user_data)
    assert response.status == 200
    used_id = response.json()['id']
    headers = {
        "X-Ya-User-Ticket": used_id,
        "Content-Type": "application/json",
    }
    bookmarks = [
        {"url": "yandex.ru", "title": "title"},
        {"url": "yandex2.ru", "title": "title", "tag": "a"},
        {"url": "yandex3.ru", "title": "title"},
        {"url": "yandex4.ru", "title": "title"},
        {"url": "yandex5.ru", "title": "title"},
        {"url": "yandex6.ru", "title": "title"},
        {"url": "yandex7.ru", "title": "title", "tag": "a"},
        {"url": "yandex8.ru", "title": "title"},
        {"url": "yandex9.ru", "title": "title"},
        {"url": "yandex10.ru", "title": "title"},
    ]
    for bookmark in bookmarks:
        await service_client.post('/v1/bookmarks', json=bookmark, headers=headers)

    data = {
        "limit": limit
    }
    if tag:
        data["tag"] = tag

    headers = {
            "X-Ya-User-Ticket": used_id,
    }
    response = await service_client.get("/v1/bookmarks", params=data, headers=headers)
    assert response.status == 200
    response_json = response.json()

    assert len(response_json['items']) == int(length)


async def test_get_bookmark(service_client):
    user_data = {"email": "me@ya.ru", "password": "pswd"}
    response = await service_client.post('/register', json=user_data)
    assert response.status == 200
    response = await service_client.post('/login', json=user_data)
    assert response.status == 200
    used_id = response.json()['id']
    headers = {
        "X-Ya-User-Ticket": used_id,
        "Content-Type": "application/json",
    }
    data = {"url": "yandex.ru", "title": "title"}
    response = await service_client.post("/v1/bookmarks", json=data, headers=headers)
    response_json = response.json()
    url_id = response_json['id']

    headers = {"X-Ya-User-Ticket": used_id}
    response = await service_client.get(f"/v1/bookmarks/{url_id}", headers=headers)
    assert response.status == 200

    response_json = response.json()
    assert response_json['url'] == data['url']


async def test_delete_bookmark(service_client):
    user_data = {"email": "me@ya.ru", "password": "pswd"}
    response = await service_client.post('/register', json=user_data)
    assert response.status == 200
    response = await service_client.post('/login', json=user_data)
    assert response.status == 200
    used_id = response.json()['id']
    headers = {
        "X-Ya-User-Ticket": used_id,
        "Content-Type": "application/json",
    }
    data = {"url": "yandex.ru", "title": "title"}
    response = await service_client.post("/v1/bookmarks", json=data, headers=headers)
    response_json = response.json()
    url_id = response_json['id']

    headers = {"X-Ya-User-Ticket": used_id}
    response = await service_client.delete(f"/v1/bookmarks/{url_id}", headers=headers)
    assert response.status == 200
