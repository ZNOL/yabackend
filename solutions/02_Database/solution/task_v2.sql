-- Укажите дополнительные индексы и команды

CREATE INDEX v2id_idx ON users_user (upper(id::text));
CREATE INDEX v2fn_idx ON users_user USING GIN (upper(first_name::text) gin_trgm_ops );
CREATE INDEX v2sn_idx ON users_user USING GIN (upper(last_name::text) gin_trgm_ops);
CREATE INDEX v2email_idx ON users_user USING GIN (upper(email::text) gin_trgm_ops );
CREATE INDEX v2phone_idx ON users_user USING GIN (upper(phone_number::text) gin_trgm_ops );

CREATE INDEX v2task2_order_index ON users_user (last_name ASC);

