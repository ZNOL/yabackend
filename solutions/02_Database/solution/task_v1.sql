CREATE INDEX id_idx ON users_user (upper(id::text));
CREATE INDEX fn_idx ON users_user USING GIN (upper(first_name::text) gin_trgm_ops );
CREATE INDEX sn_idx ON users_user USING GIN (upper(second_name::text) gin_trgm_ops);
CREATE INDEX lt_idx ON users_user USING GIN (upper(last_name::text) gin_trgm_ops);
CREATE INDEX email_idx ON users_user USING GIN (upper(email::text) gin_trgm_ops );
