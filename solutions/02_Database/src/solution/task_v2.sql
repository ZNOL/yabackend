-- Укажите дополнительные индексы и команды
CREATE INDEX task_v12_btree ON users_user (first_name, second_name, last_name, email);
CREATE INDEX task_v12_btree ON users_user (upper(first_name), upper(second_name), upper(last_name), upper(email));

CREATE INDEX task2_order_index ON users_user (last_name NULLS FIRST);

