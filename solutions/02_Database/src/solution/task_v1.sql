CREATE INDEX task_v1_btree ON users_user (first_name, second_name, last_name, email);
CREATE INDEX task_v1_btree ON users_user (upper(first_name), upper(second_name), upper(last_name), upper(email));
