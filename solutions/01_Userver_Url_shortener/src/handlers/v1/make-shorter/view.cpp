#include "view.hpp"

#include <chrono>

#include <fmt/format.h>

#include <userver/formats/json.hpp>
#include <userver/server/handlers/http_handler_base.hpp>
#include <userver/storages/postgres/cluster.hpp>
#include <userver/storages/postgres/component.hpp>
#include <userver/utils/assert.hpp>
#include <userver/utils/datetime.hpp>

namespace url_shortener {

namespace {
using ll = long long;
using ull = unsigned long long;

ull getMultiplayer(std::string& s) {
  switch (s[0]) {
    case 'S':
      return 1;
    case 'M':
      return 60;
    case 'H':
      return 3600;
    case 'D':
      return 86400;
    default:
      return 0;
  }
}

class UrlShortener final : public userver::server::handlers::HttpHandlerBase {
 public:
  static constexpr std::string_view kName = "handler-v1-make-shorter";

  UrlShortener(const userver::components::ComponentConfig& config,
               const userver::components::ComponentContext& component_context)
      : HttpHandlerBase(config, component_context),
        pg_cluster_(
            component_context
                .FindComponent<userver::components::Postgres>("postgres-db-1")
                .GetCluster()) {}

  std::string HandleRequestThrow(
      const userver::server::http::HttpRequest& request,
      userver::server::request::RequestContext&) const override {
    auto request_body =
        userver::formats::json::FromString(request.RequestBody());

    auto url = request_body["url"].As<std::optional<std::string>>();
    if (!url.has_value()) {
      auto& response = request.GetHttpResponse();
      response.SetStatus(userver::server::http::HttpStatus::kBadRequest);
      return {};
    }

    auto vip_key = request_body["vip_key"].As<std::optional<std::string>>();
    auto time_to_live = request_body["time_to_live"].As<std::optional<ull>>();
    auto time_to_live_unit =
        request_body["time_to_live_unit"].As<std::optional<std::string>>();

    userver::formats::json::ValueBuilder response;
    if (vip_key.has_value()) {
      auto vip_id = vip_key.value();
      auto time = time_to_live.value_or(10);
      auto time_unit = time_to_live_unit.value_or("HOURS");

      using namespace std::chrono;
      // uint64_t sec =
      // duration_cast<seconds>(system_clock::now().time_since_epoch()).count();
      uint64_t sec = duration_cast<seconds>(
                         userver::utils::datetime::Now().time_since_epoch())
                         .count();

      uint64_t duration = time * getMultiplayer(time_unit);
      if (duration == 0 || duration > 172800) {
        auto& response = request.GetHttpResponse();
        response.SetStatus(userver::server::http::HttpStatus::kBadRequest);
        return {};
      }

      auto existence_check = pg_cluster_->Execute(
          userver::storages::postgres::ClusterHostType::kMaster,
          "SELECT EXISTS (SELECT * FROM url_shortener.urls "
          "WHERE (time_to_live >= cast($1 AS BIGINT) "
          "AND id = $2))",
          std::to_string(sec), vip_id);

      if (existence_check.AsSingleRow<bool>()) {
        auto& response = request.GetHttpResponse();
        response.SetStatus(userver::server::http::HttpStatus::kBadRequest);
        return {};
      }

      auto result = pg_cluster_->Execute(
          userver::storages::postgres::ClusterHostType::kMaster,
          "WITH ins AS ("
          "INSERT INTO url_shortener.urls(id, url, time_to_live) VALUES($1, "
          "$2, cast($3 AS BIGINT)) "
          "ON CONFLICT (url) DO "
          "UPDATE SET (time_to_live, url) = (cast($3 AS BIGINT), $2) "
          "RETURNING urls.id ) "
          //"SELECT id FROM url_shortener.urls WHERE url = $2 "
          //"UNION ALL "
          "SELECT id FROM ins",
          vip_id, url.value(), std::to_string(sec + duration));

      response["short_url"] = fmt::format("http://localhost:8080/{}",
                                          result.AsSingleRow<std::string>());

      // LOG_INFO() << std::to_string(sec) << " "
      //<< std::to_string(sec + duration);

    } else {
      auto result = pg_cluster_->Execute(
          userver::storages::postgres::ClusterHostType::kMaster,
          "WITH ins AS ( "
          "INSERT INTO url_shortener.urls(url) VALUES($1) "
          "ON CONFLICT DO NOTHING "
          "RETURNING urls.id "
          ") "
          "SELECT id FROM url_shortener.urls WHERE url = $1 "
          "UNION ALL "
          "SELECT id FROM ins",
          url.value());

      response["short_url"] = fmt::format("http://localhost:8080/{}",
                                          result.AsSingleRow<std::string>());
    }

    return userver::formats::json::ToString(response.ExtractValue());
  }

  userver::storages::postgres::ClusterPtr pg_cluster_;
};

}  // namespace

void AppendUrlShortener(userver::components::ComponentList& component_list) {
  component_list.Append<UrlShortener>();
}

}  // namespace url_shortener
