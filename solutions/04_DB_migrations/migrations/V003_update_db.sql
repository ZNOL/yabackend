ALTER TABLE driver ADD full_name VARCHAR(255) NOT NULL DEFAULT ' ';
UPDATE driver SET full_name = CONCAT(name, ' ', last_name) WHERE last_name IS NOT NULL;
UPDATE driver SET full_name = name WHERE last_name IS NULL;

ALTER TABLE driver ALTER column full_name DROP DEFAULT;
ALTER TABLE driver DROP name;
ALTER TABLE driver DROP last_name;

ALTER TABLE spaceship_manufacturer ADD moex_code VARCHAR(53);
UPDATE spaceship_manufacturer SET moex_code = CONCAT(nasdaq_code, '_ru') WHERE nasdaq_code IS NOT NULL;
UPDATE spaceship_manufacturer SET moex_code = nasdaq_code WHERE nasdaq_code IS NULL;
ALTER TABLE spaceship_manufacturer DROP nasdaq_code;

