CREATE TABLE IF NOT EXISTS spaceship_manufacturer (
    id INT PRIMARY KEY,
    company_name VARCHAR(50) NOT NULL, 
    country VARCHAR(50),
    nasdaq_code VARCHAR(50)
);

CREATE TABLE IF NOT EXISTS spaceship_model (
    id INT PRIMARY KEY,
    manufacturer_id INT NOT NULL REFERENCES spaceship_manufacturer(id),
    model_name VARCHAR(50),
    horsepower FLOAT NOT NULL CHECK(horsepower >= 170000000 AND horsepower <= 240000000)
);

CREATE TABLE IF NOT EXISTS spaceship (
    id INT PRIMARY KEY,
    ship_number VARCHAR(50) NOT NULL,
    model_id INT NOT NULL REFERENCES spaceship_model(id)
);

CREATE TABLE IF NOT EXISTS driver (
    id INT PRIMARY KEY,
    name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50),
    login VARCHAR(50) NOT NULL,
    city VARCHAR(30)
);

CREATE TABLE IF NOT EXISTS spaceship_rent (
    driver_id INT NOT NULL REFERENCES driver(id),
    spaceship_id INT NOT NULL REFERENCES spaceship(id),
    rent_start TIMESTAMP NOT NULL,
    rent_end TIMESTAMP NOT NULL
);

