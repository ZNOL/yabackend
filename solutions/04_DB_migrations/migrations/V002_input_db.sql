COPY driver 
    FROM '/home/runner/builds/ivan-melnikov-1/db_migrations/resources/init_data/driver.csv'
    WITH CSV HEADER;
    
COPY spaceship_manufacturer 
    FROM '/home/runner/builds/ivan-melnikov-1/db_migrations/resources/init_data/spaceship_manufacturer.csv'
    WITH CSV HEADER;
    
COPY spaceship_model
    FROM '/home/runner/builds/ivan-melnikov-1/db_migrations/resources/init_data/spaceship_model.csv'
    WITH CSV HEADER;
    
COPY spaceship 
    FROM '/home/runner/builds/ivan-melnikov-1/db_migrations/resources/init_data/spaceship.csv'
    WITH CSV HEADER;
    
COPY spaceship_rent 
    FROM '/home/runner/builds/ivan-melnikov-1/db_migrations/resources/init_data/spaceship_rent.csv'
    WITH CSV HEADER;
    
