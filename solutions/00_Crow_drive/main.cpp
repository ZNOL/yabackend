#include <string>
#include <vector>
#include <iostream>

#include <crow.h>

#include <jdbc/mysql_connection.h>
#include <jdbc/cppconn/driver.h>
#include <jdbc/cppconn/exception.h>
#include <jdbc/cppconn/prepared_statement.h>

using namespace std;

int main() {
    cout << start << endl;

    crow::SimpleApp app;

    CROW_ROUTE(app, "/health")([](){
        return "All right!";
    });

    CROW_ROUTE(app, "/api/blogs").methods(crow::HTTPMethod::POST)(
    [&](const crow::request& req) {
        auto body = crow::json::load(req.body);
        if (!body) {
            return crow::response(400, "Невалидная схема документа или входные данные не верны.");
        }

    });

    CROW_ROUTE(app, "/nodes/<string>").methods("GET"_method, "PATCH"_method)(
    [](string s) {
        cout << s << endl;
        return s + s;
    });

    app.port(80).multithreaded().run();

    return 0;
}
