#pragma once

#include <string>
#include <string_view>

#include <userver/components/component_list.hpp>

namespace code_architecture {

enum class MyUserType {
    kStranger,
    kUser,
    kDoctor,
};

enum class MyProductType {
    kUnknown,
    kCommon,
    kSpecial,
    kReceipt,
};

void AppendCheck(userver::components::ComponentList& component_list);

}  // namespace code_architecture
