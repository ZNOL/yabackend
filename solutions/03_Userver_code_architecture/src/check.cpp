#include "check.hpp"

#include <string>
// #include <utility>
#include <unordered_map>
#include <unordered_set>

#include <fmt/format.h>

#include <userver/clients/dns/component.hpp>
#include <userver/server/handlers/http_handler_base.hpp>
#include <userver/storages/postgres/cluster.hpp>
#include <userver/storages/postgres/component.hpp>
#include <userver/utils/assert.hpp>

#include <userver/testsuite/testsuite_support.hpp>
#include <userver/utest/using_namespace_userver.hpp>

#include <userver/components/minimal_server_component_list.hpp>
#include <userver/server/handlers/http_handler_base.hpp>
#include <userver/utils/daemon_run.hpp>

#include <userver/storages/postgres/cluster.hpp>
#include <userver/storages/postgres/component.hpp>

namespace code_architecture {

namespace {
  const storages::postgres::Query kCheckUser {
    "SELECT id FROM code_architecture.user_account WHERE id=cast($1 AS integer) ",
        storages::postgres::Query::Name{"select_user"},
  };

  const storages::postgres::Query kCheckDoctor {
     "SELECT id FROM code_architecture.doctor_account WHERE id=cast($1 AS integer) ",
      storages::postgres::Query::Name{"select_doctor"},
  };

  const storages::postgres::Query kCheckCommonItem {
      "SELECT id FROM code_architecture.common_item WHERE id=cast($1 AS integer) ",
      storages::postgres::Query::Name{"select_common_item"},
  };

  const storages::postgres::Query kCheckSpecialItem {
      "SELECT id FROM code_architecture.special_item WHERE id=cast($1 AS integer) ",
      storages::postgres::Query::Name{"select_special_item"},
  };

  const storages::postgres::Query kCheckReceiptItem {
      "SELECT id FROM code_architecture.receipt_item WHERE id=cast($1 AS integer) ",
      storages::postgres::Query::Name{"select_receipt_item"},
  };

  const storages::postgres::Query kCheckUserReceipt {
      "SELECT id FROM code_architecture.receipt_item WHERE user_id=cast($1 AS integer) "
      "AND item_id=cast($2 AS integer) ",
      storages::postgres::Query::Name{"select_user_receipt"},
  };

  const storages::postgres::Query kCheckDoctorSpecial {
      "SELECT specialty_id FROM code_architecture.doctor_account AS t1 WHERE t1.id=cast($1 AS integer) "
      "EXCEPT "
      "SELECT specialty_id FROM code_architecture.special_item AS t2 WHERE t2.id=cast($2 AS integer) ",
      storages::postgres::Query::Name{"select_doctor_special"},
  };

  std::vector<std::pair<storages::postgres::Query, MyUserType>> queries {
      {kCheckUser, MyUserType::kUser},
      {kCheckDoctor, MyUserType::kDoctor},
  };

  std::unordered_map<std::string, storages::postgres::Query> pr_queries {
      {"common", kCheckCommonItem},
      {"special", kCheckSpecialItem},
      {"receipt", kCheckReceiptItem},
  };

  std::unordered_map<std::string, std::string> no_user_error {
      {"common", "NO_USER"},
      {"special", "NO_USER_SPECIAL_ITEM"},
      {"receipt", "NO_USER_NO_RECEIPT"},
  };

  std::pair<std::string, std::string> splitBy(std::string s, std::string del) {
    std::pair<std::string, std::string> ans;
    auto pos = s.find(del);
    if (pos != std::string::npos) {
      ans.first = s.substr(0, pos);
      s.erase(0, pos + del.length());
      ans.second = s;
    }
    return ans;
  }

class Check final : public userver::server::handlers::HttpHandlerBase {
 public:
  static constexpr std::string_view kName = "handler-check";

  Check(const userver::components::ComponentConfig& config,
        const userver::components::ComponentContext& component_context)
      : HttpHandlerBase(config, component_context),
        pg_cluster_(
            component_context
                .FindComponent<userver::components::Postgres>("postgres-db-1")
                .GetCluster()) {}

  std::string HandleRequestThrow(
      const userver::server::http::HttpRequest& request,
      userver::server::request::RequestContext&) const override {

    const auto& user_id = request.GetArg("user_id");
    const auto &item_id = request.GetArgVector("item_id");
    if (user_id.empty() || item_id.empty()) {
      throw server::handlers::ClientError(
          server::handlers::ExternalBody{"Not enough arguments"});
    }

    size_t idx = 0;
    auto user_type = MyUserType::kStranger;
    while (user_type == MyUserType::kStranger && idx < code_architecture::queries.size()) {
      auto res = pg_cluster_->Execute(
          storages::postgres::ClusterHostType::kSlave, code_architecture::queries[idx].first, user_id);
      if (!res.IsEmpty()) {
        user_type = queries[idx].second;
      }
      ++idx;
    }

    std::vector<std::pair<std::string, std::string>> ans;
    for (auto item : item_id) {
      std::transform(item.begin(), item.end(), item.begin(), [](unsigned char c){ return std::tolower(c); });
      auto temp = splitBy(item, "_");

      if (code_architecture::pr_queries.find(temp.first) == code_architecture::pr_queries.end()) {  // WRONG_CATEGORY
        ans.push_back({item, "WRONG_CATEGORY"});
        continue;
      }

      int64_t id = -1;
      try {
        size_t pos;
        id = std::stol(temp.second, &pos);
      } catch (const std::exception &ex) {   // INCORRECT_ITEM_ID
        ans.push_back({item, "INCORRECT_ITEM_ID"});
        continue;
      }

      auto res = pg_cluster_->Execute(
          storages::postgres::ClusterHostType::kSlave, code_architecture::pr_queries[temp.first], user_id);
      if (res.IsEmpty()) { // ITEM_NOT_FOUND
        ans.push_back({item, "ITEM_NOT_FOUND"});
        continue;
      } else if (user_type == MyUserType::kStranger) {  // NO_USER
        ans.push_back({item, code_architecture::no_user_error[temp.first]});
        continue;
      }

      if (temp.first == "special" && user_type == MyUserType::kUser) { // ITEM_IS_SPECIAL
        ans.push_back({item, "ITEM_IS_SPECIAL"});
        continue;
      } else if (temp.first == "special" && user_type == MyUserType::kDoctor) {
        auto res = pg_cluster_->Execute(
            storages::postgres::ClusterHostType::kSlave, kCheckDoctorSpecial, user_id, std::to_string(id));

        if (!res.IsEmpty()) {  // ITEM_SPECIAL_WRONG_SPECIFIC
          ans.push_back({item, "ITEM_SPECIAL_WRONG_SPECIFIC"});
          continue;
        }
      }

      if (temp.first == "receipt") {
        auto res = pg_cluster_->Execute(
            storages::postgres::ClusterHostType::kSlave, kCheckUserReceipt, user_id, std::to_string(id));

        if (res.IsEmpty()) {  // NO_RECEIPT
          ans.push_back({item, "NO_RECEIPT"});
          continue;
        }
      }
    }

    userver::formats::json::ValueBuilder response = userver::formats::json::ValueBuilder(userver::formats::common::Type::kArray);

    userver::formats::json::ValueBuilder temp;
    for (auto &i : ans) {
      temp["item_id"] = i.first;
      temp["problem"] = i.second;
    }
    response.PushBack(std::move(temp));

    return userver::formats::json::ToString(response.ExtractValue());
  }

  userver::storages::postgres::ClusterPtr pg_cluster_;
};

}  // namespace

void AppendCheck(userver::components::ComponentList& component_list) {
  component_list.Append<Check>();
  component_list.Append<userver::components::Postgres>("postgres-db-1");
  component_list.Append<userver::clients::dns::Component>();
}

}  // namespace code_architecture
